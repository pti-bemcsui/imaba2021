import React from 'react'
import { Img } from 'react-image'
import { Link } from 'react-router-dom'

const directory = [
    { title: "Info Kampus", imgName: "InfoKampus.svg", to: "/", class: "kampus" },
    { title: "Info Akademik", imgName: "InfoAkademik.svg", to: "/pacil/akademik", class: "akademik" },
    { title: "Info Non-Akademik", imgName: "InfoNonAkademik.svg", to: "/pacil/non-akademik", class: "fasilkom" },
    { title: "Kanal Narahubung", imgName: "KanalNarahubung.svg", to: "/pacil/narahubung", class: "narahubung" },
]

export default function hero() {
    return (
        <div style={{ minHeight: "80vh", paddingTop: 80, backgroundColor: "#FDF7FF", width: "100%", display: "flex", justifyContent: "center", alignItems: "center", flexDirection: "column", height: "100%" }} className="hero-pacil">
            <Img alt="" className="image-hero" src={require("../../assets/iconlogo/PagePacilHeroDecLeft.svg")} style={{ width: "fit-content", position: "absolute", left: 0, }} />
            <Img alt="" className="image-hero" src={require("../../assets/iconlogo/PagePacilHeroDecRight.svg")} style={{ width: "fit-content", position: "absolute", right: 0, }} />

            <div style={{ display: "flex", flexDirection: "column", alignItems: "center", gap: 8, zIndex: 50, paddingTop: 24, paddingBottom: 24 }}>
                <div style={{ display: "flex", alignItems: "flex-end", gap: 4 }}>
                    <Img alt="" src={require("../../assets/iconlogo/ImabaText.svg")} style={{ width: "fit-content" }} />
                    <Img alt="" src={require("../../assets/iconlogo/PacilText.svg")} style={{ width: "fit-content" }} />
                </div>
                <p style={{ maxWidth: 400, textAlign: "center", color: "#352B48" }}>
                    iMaba hadir untuk memberikan informasi-informasi penting dan menarik seputar akademik, kampus UI, Fasilkom, serta kanal informasi yang dapat kamu hubungi.
                </p>
            </div>
            <div style={{ display: "flex", gap: 16, justifyContent: "center", flexWrap: "wrap", zIndex: 50 }}>
                {directory.map((item, key) => (
                    <Link to={item.to} style={{ display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center", gap: 6, textDecoration: "none" }} key={key} className = "p-hover-man">
                        <Img alt={item.class} src={require(`../../assets/iconlogo/${item.imgName}`)} className="card-animation" style={{ width: 200 }} />
                        <p style={{margin: 0, padding: 0, color: "#352B48", fontWeight: "normal"}}> {item.title} </p>
                    </Link>
                ))}
            </div>
        </div>
    )
}