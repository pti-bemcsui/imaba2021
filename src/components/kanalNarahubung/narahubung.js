import React, { Component } from 'react'

import Card from 'react-bootstrap/Card'
import CardDeck from 'react-bootstrap/CardDeck'
import Carousel from 'react-bootstrap/Carousel'
import { Img } from 'react-image'

import Styles from "../../styles/infoAkademik"
import Spinner from 'react-bootstrap/Spinner'

import kanal from '../../data/kanalInfo.json'

export default class Narahubung extends Component {
    state = { term: 1 }
    spinner = <Spinner variant="warning" animation="border" />
    matkulCard = (logo, title, desc, contacts) => {
        return (
            <Card key={title} className="subcard" style={{ width: "100%" }}>
                <div className="header" style={{ display: "flex", flexDirection: "column", gap: 12 }}>
                    <div style={{ display: "flex", gap: 8, alignItems: "center" }}>
                        <Img alt="" src={require(`../../assets/iconlogo/${logo}`)} style={{ width: 70, height: 75, objectFit: "contain", paddingBottom: 8 }} loader={this.spinner} />
                        <h3 style={{ fontWeight: "bold", color: "#352B48" }} className = "title-narahubung">{title}</h3>
                    </div>
                    <div className="flex-normal flex-center" style={{ gap: 16, display: "flex", flexDirection: "column" }}>
                        {contacts[0].narahubung.length ? <div style={{ display: "flex", flexDirection: "column", gap: "1em" }}>
                            <div style={{ display: "flex", gap: 8, backgroundColor: "#E5DFEE", padding: "1em", alignItems: "center", borderRadius: 4 }}>
                                <Img alt="" src={require(`../../assets/iconlogo/UserGroup.svg`)} style={{ width: 30, height: 30, alignItems: "center" }} loader={this.spinner} />
                                <h3 style={{ color: "#534764", margin: 0, padding: 0, fontWeight: "bold" }} classname = "title-sec"> Narahubung </h3>
                            </div>
                            {contacts[0].narahubung.map((item, id) => (
                                <div style={{ display: "flex", flexDirection: "column", gap: 8 }} key={id}>
                                    <h3 style={{ color: "#534764", margin: 0, padding: 0, }}> {item.nama} </h3>
                                    <Chips imageIcon={<Img alt="" src={require(`../../assets/iconlogo/PhoneIcon.svg`)} style={{ width: 15, height: 15, }} loader={this.spinner} />
                                    }> {item.hp} </Chips>
                                    <Chips
                                        imageIcon={<Img alt="" src={require(`../../assets/iconlogo/LineIcon.svg`)} style={{ width: 15, height: 15, }} loader={this.spinner} />
                                        }> {item.line} </Chips>

                                </div>
                            ))}
                        </div> : null}
                        {Object.keys(contacts[1].socmed).length ? <div style={{ display: "flex", flexDirection: "column", gap: "1em" }}>
                            <div style={{ display: "flex", gap: 8, backgroundColor: "#E5DFEE", padding: "1em", alignItems: "center" }}>
                                <Img alt="" src={require(`../../assets/iconlogo/SpeakerPhone.svg`)} style={{ width: 30, height: 30, alignItems: "center" }} loader={this.spinner} />
                                <h3 style={{ color: "#534764", margin: 0, padding: 0, fontWeight: "bold", borderRadius: 4 }} classname = "title-sec"> Media Sosial </h3>
                            </div>
                            {contacts[1].socmed.twitter && <Chips imageIcon={<Img alt="" src={require(`../../assets/iconlogo/TwitterIcon.svg`)} style={{ width: 15, height: 15, }} loader={this.spinner} />
                            }> {contacts[1].socmed.twitter} </Chips>}
                            {contacts[1].socmed.ig && <Chips imageIcon={<Img alt="" src={require(`../../assets/iconlogo/InstagramIcon.svg`)} style={{ width: 15, height: 15, }} loader={this.spinner} />
                            }> {contacts[1].socmed.ig} </Chips>}
                            {contacts[1].socmed.fb && <Chips imageIcon={<Img alt="" src={require(`../../assets/iconlogo/FacebookIcon.svg`)} style={{ width: 15, height: 15, }} loader={this.spinner} />
                            }> {contacts[1].socmed.fb} </Chips>}
                            {contacts[1].socmed.web && <Chips imageIcon={<Img alt="" src={require(`../../assets/iconlogo/WebIcon.svg`)} style={{ width: 15, height: 15, }} loader={this.spinner} />
                            }> {contacts[1].socmed.web} </Chips>}
                            {contacts[1].socmed.line && <Chips imageIcon={<Img alt="" src={require(`../../assets/iconlogo/LineIcon.svg`)} style={{ width: 15, height: 15, }} loader={this.spinner} />
                            }> {contacts[1].socmed.line} </Chips>}
                            {contacts[1].socmed.hp && <Chips imageIcon={<Img alt="" src={require(`../../assets/iconlogo/PhoneIcon.svg`)} style={{ width: 15, height: 15, }} loader={this.spinner} />
                            }> {contacts[1].socmed.hp} </Chips>}
                        </div> : null}
                    </div>
                </div>
            </Card>
        )
    }

    displayCarousel = (term) => {
        return (
            <div>
                <Carousel controls indicators className="carousel-xl">
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            {term.slice(0, 2).map(item => this.matkulCard(item.logo, item.title, item.desc, item.contacts))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            {term.slice(2, 4).map(item => this.matkulCard(item.logo, item.title, item.desc, item.contacts))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            {term.slice(4, 6).map(item => this.matkulCard(item.logo, item.title, item.desc, item.contacts))}
                        </CardDeck>
                    </Carousel.Item>
                </Carousel>
                <Carousel controls indicators className="carousel-lg">
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            {term.slice(0, 2).map(item => this.matkulCard(item.logo, item.title, item.desc, item.contacts))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            {term.slice(2, 4).map(item => this.matkulCard(item.logo, item.title, item.desc, item.contacts))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            {term.slice(4, 6).map(item => this.matkulCard(item.logo, item.title, item.desc, item.contacts))}
                        </CardDeck>
                    </Carousel.Item>
                </Carousel>
                <Carousel controls={false} indicators className="carousel-md">
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            {term.slice(0, 2).map(item => this.matkulCard(item.logo, item.title, item.desc, item.contacts))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            {term.slice(2, 4).map(item => this.matkulCard(item.logo, item.title, item.desc, item.contacts))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            {term.slice(4, 6).map(item => this.matkulCard(item.logo, item.title, item.desc, item.contacts))}
                        </CardDeck>
                    </Carousel.Item>

                </Carousel>
                <Carousel controls={false} indicators className="carousel-sm">
                    {term.map(item => (
                        <Carousel.Item key={item.name}>
                            {this.matkulCard(item.logo, item.title, item.desc, item.contacts)}
                        </Carousel.Item>
                    ))}
                </Carousel>
            </div>
        )
    }

    displayTerm = () => {
        const { info } = kanal
        return this.displayCarousel(info)
    }

    render() {
        return (
            <Styles>
                <div style={{ width: "100%", backgroundColor: "transparent", borderRadius: 4, gap: 32, padding: 32, alignItems: "center" }} className="flex-tight flex-center" id="narahubung">
                    <div style={{ display: "flex", flexDirection: "column", gap: 8 }}>
                        {this.displayTerm()}
                    </div>
                </div>
            </Styles>
        )
    }
}


const Chips = ({ imageIcon, children }) => (
    <div style={{ borderRadius: 30, border: "2px solid #3271ED", width: "fit-content", display: "flex", alignItems: "center", padding: "6px 12px" }}>
        {imageIcon}
        <p style={{ color: "#3271ED", padding: 0, margin: 0, borderLeft: "1px solid #3271ED", marginLeft: 6, paddingLeft: 6 }}> {children} </p>
    </div>
)
