import React from 'react'
import Navbar from 'react-bootstrap/Navbar'
import {Img} from 'react-image'
import data from '../data/footer.json'

export default function footer() {
    return (
        <Navbar style={{backgroundColor: "#352B48", paddingTop: 16, paddingBottom: 16, paddingLeft: 32, paddingRight: 32, width: "100%", position: "relative", bottom: 0,}} expand="xl" className="footer-media-query d-flex justify-content-between"> 
           <div style={{display: "flex", gap: 8, alignItems: "center"}} className = "footer-media-query-2">
            <Img alt="" src={ require("../assets/iconlogo/PTI.svg") } height={60} width={60}/>
            <div style={{display: "flex", flexDirection: "column", gap: 4, textAlign: "center"}}>
                <h1 style={{fontSize: 12, color: "#FDF7FF", padding: 0, margin: 0}}> Dikembangkan oleh biro PTI & Media </h1>
                <h1 style={{fontSize: 12, color: "#E5DFEE", padding: 0, margin: 0}}> BEM FASILKOM UI 2023 </h1>
            </div>
           </div>
           <div style={{display: "flex", gap: "1em"}}>
            {data.map((item, key) => (
                    <a key = {key} href = {item.link} target='_blank' rel="noopener noreferrer">
                        <Img alt={item.name} src={ require(`../assets/iconlogo/${item.logo}`) } height={20} width={20}/>
                    </a>
            ))}
           </div>
        </Navbar>
    )
}
