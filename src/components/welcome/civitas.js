import React, { Component } from 'react'

import { Img } from 'react-image'
import Badge from 'react-bootstrap/Badge'
import Card from 'react-bootstrap/Card'
import CardDeck from 'react-bootstrap/CardDeck'
import Carousel from 'react-bootstrap/Carousel'
import Spinner from 'react-bootstrap/Spinner'

import Styles from "../../styles/infoAkademik"

import data from '../../data/kampus-fitur.json'

export default class Civitas extends Component {
    state = { term: 1 }
    spinner = <Spinner variant="warning" animation="border" />

    matkulCard = (name, title, desc, link, prev) => {
        return (
            <Card key={name} className="subcard" style={{width: "100%"}}>
                <div className="header" style={{display: "flex", flexDirection: "column"}}>
                    <Img alt="" src={ require(`../../assets/infoKampus/fitur-${name}.png`)} style={{width:100, height: 100, margin: "auto", paddingBottom: 8}} loader={this.spinner}/>

                    <h3 style={{fontWeight: "bold", color: "#352B48", marign: 0, padding: 0}}>{title}</h3>
                    <div className="badgewrapper" style={{gap: 8, display: "flex", flexWrap: "wrap"}}>
                        <a href={link} target="_blank" rel="noopener noreferrer">
                            <Badge variant="success"> {prev} </Badge>
                        </a>
                    </div>                    
                </div>
                <p style={{color: "#534764"}}>{desc}</p>
            </Card>
        )
    }

    displayCarousel = (term) => {
        return (
            <div>
                <Carousel controls indicators className="carousel-xl">
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            { term.slice(0,3).map( item => this.matkulCard(item.name, item.title, item.desc, item.link, item.prev))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            { term.slice(3,6).map( item => this.matkulCard(item.name, item.title, item.desc, item.link, item.prev, item.prev))}
                        </CardDeck>
                    </Carousel.Item>
                </Carousel>
                <Carousel controls indicators className="carousel-lg">
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            { term.slice(0,2).map( item => this.matkulCard(item.name, item.title, item.desc, item.link, item.prev))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            { term.slice(2,4).map( item => this.matkulCard(item.name, item.title, item.desc, item.link, item.prev))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            { term.slice(4,6).map( item => this.matkulCard(item.name, item.title, item.desc, item.link, item.prev))}
                        </CardDeck>
                    </Carousel.Item>
                </Carousel>
                <Carousel controls={false} indicators className="carousel-md">
                <Carousel.Item>
                        <CardDeck className="my-3">
                            { term.slice(0,2).map( item => this.matkulCard(item.name, item.title, item.desc, item.link, item.prev))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            { term.slice(2,4).map( item => this.matkulCard(item.name, item.title, item.desc, item.link, item.prev))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            { term.slice(4,6).map( item => this.matkulCard(item.name, item.title, item.desc, item.link, item.prev))}
                        </CardDeck>
                    </Carousel.Item>
                </Carousel>
                <Carousel controls={false} indicators className="carousel-sm">
                    { term.map( item => (
                        <Carousel.Item key={item.title}>
                            {this.matkulCard(item.name, item.title, item.desc, item.link, item.prev)}
                        </Carousel.Item>
                    ))}
                </Carousel>
            </div>
        )
    }

    displayTerm = () => {
        const { fitur } = data
        return this.displayCarousel(fitur)
    }

    render() {
        return (
            <Styles>
                <div style={{width: "100%", backgroundColor: "#FFFEF7", borderRadius: 4, gap: 32, padding: 32, alignItems: "center", display: "flex", flexDirection: "column"}} id = "civitas-academica">
                    <div className = "flex-tight flex-center" style={{gap: 32, alignItems: "center"}}>
                        <Img alt="" src={ require(`../../assets/welcomePage/FiturCivitas.svg`)} style={{width:"100%", height: "100%"}} loader={this.spinner} className='w-35'/>
                        <div style={{display: "flex", flexDirection: "column", gap: 8}} className = "w-65">
                            <h2 style={{color: "#352B48", padding: 0, margin: 0}} className = "real-h2 h2-center"> Fitur-Fitur </h2>
                            <h2 style={{color: "#352B48", padding: 0, margin: 0}} className = "real-h2 main-span h2-center"> Civitas Academica Universitas Indonesia </h2>
                        </div>
                    </div>
                    {this.displayTerm()}
                </div>
            </Styles>
        )
    }
}
