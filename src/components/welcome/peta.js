import React, { Component } from 'react'

import { Img } from 'react-image'
import Spinner from 'react-bootstrap/Spinner'

export default class Peta extends Component {
    state = { slide: "tempat"}
    spinner = <Spinner variant="warning" animation="border" />

    render() {

        return (
            <div style={{display: "flex", flexDirection: "column", width: "100%", backgroundColor: "#FFFEF7", borderRadius: 4, gap: 32, padding: 32}}>
                <div style={{display: "flex", padding: 32, alignItems: "center", gap: 32}} id = "peta">
                    <Img alt = "peta" src = { require("../../assets/welcomePage/Peta.svg" )} style={{overflow: "hidden"}}/>
                    <div style={{display: 'flex', gap: 8}}>
                        <div style={{display: "flex", gap: 8, flexDirection: 'column', }}>
                            <h2 className = "bigger-h2" style={{color: "#352B48",}}> Peta </h2>
                            <h2 className = "real-h2 main-span" > Universitas Indonesia </h2>
                            <div className = "flex-small" style={{display: "flex", gap: 8}}>
                                <Chips active={this.state.slide === "tempat"} cb={() => this.setState({slide:"tempat"})}> Tempat </Chips>
                                <Chips active={this.state.slide === "transportasi"} cb={() => this.setState({slide:"transportasi"})}> Transportasi </Chips>
                                <Chips active={this.state.slide === "bikun"} cb={() => this.setState({slide:"bikun"})}> Bikun </Chips>
                            </div>
                        </div>
                    </div>
                </div>
                <div className = "flex-normal flex-center" style={{width: "100%", marginBottom: 32, display: "flex", borderRadius: 4, border: "1px solid #EEEEDF", gap: 16, padding: 16}}>
                    {this.state.slide === "tempat" && (
                        <Img alt = "tempat" src = { require("../../assets/infoKampus/Denah-UI.svg" )} style={{overflow: "hidden"}} className = "w-50"  loader={this.spinner}/>
                    )}
                    {this.state.slide === "transportasi" && (
                        <Img alt = "transportasi" src = { require("../../assets/infoKampus/peta-transport.png" )} style={{overflow: "hidden"}} className = "w-65" loader={this.spinner}/>
                    )}
                    {this.state.slide === "bikun" && (
                        <Img alt = "bikun" src = { require("../../assets/infoKampus/peta-bikun.svg" )} style={{overflow: "hidden"}} className = "w-65" loader={this.spinner}/>
                    )}
                    {this.state.slide === "bikun" && (
                        <div style={{display: "flex", flexDirection: "column", gap: 16, backgroundColor: "#EEEEDF", padding: 16}} className = "w-35-new">
                            <h3 style={{margin:0, padding: 0, fontSize: 32, color: "#352B48", fontWeight: "bold"}}> Rute Bikun </h3>
                            <div style={{ display: "flex", gap: 8, backgroundColor: "#DEDEB8", padding: "1em", alignItems: "center", justifyContent: "center" }}>
                                <Img alt="" src={require(`../../assets/iconlogo/clock.svg`)} style={{ width: 25, height: 25, alignItems: "center" }} loader={this.spinner} />
                                <h3 style={{ color: "#534764", margin: 0, padding: 0, fontWeight: "bold", fontSize: 18 }}> Waktu Operasional </h3>
                            </div>
                            <div style={{display: "flex", flexDirection: "column", gap: 4}}>
                                <p style={{margin: 0, padding: 0, color: "#352B48", fontWeight: "bold", display: "flex", flexWrap: "wrap", gap: 8, alignItems: "center"}}> Senin – Jumat <span style={{color: "#DEDEB8", fontWeight: "bold"}}> | </span> 07.00 – 21.00 WIB </p>
                                <p style={{margin: 0, padding: 0, color: "#352B48", fontWeight: "bold", display: "flex", flexWrap: "wrap", gap: 8, alignItems: "center"}}> Sabtu <span style={{color: "#DEDEB8", fontWeight: "bold"}}> | </span> 07.00 – 15.00 WIB </p>
                            </div>
                            <div style={{ display: "flex", gap: 8, backgroundColor: "#DEDEB8", padding: "1em", alignItems: "center", justifyContent: "center" }}>
                                <Img alt="" src={require(`../../assets/iconlogo/document-text.svg`)} style={{ width: 25, height: 25, alignItems: "center" }} loader={this.spinner} />
                                <h3 style={{ color: "#534764", margin: 0, padding: 0, fontWeight: "bold", fontSize: 18 }}> Keterangan </h3>
                            </div>
                            <div style={{display: "flex", flexDirection: "column", gap: 4}}>
                                <div style={{display: "flex", gap: 8, alignItems: "center"}}> 
                                    <Img alt="" src={require(`../../assets/infoKampus/JalurBiru.svg`)} style={{ width: 50, height: 25, alignItems: "center" }} loader={this.spinner} />
                                    <p style={{margin: 0, padding: 0, color: "#352B48", fontWeight: "bold"}}> Jalur Biru </p>
                                </div>
                                <div style={{display: "flex", gap: 8, alignItems: "center"}}> 
                                    <Img alt="" src={require(`../../assets/infoKampus/JalurMerah.svg`)} style={{ width: 50, height: 25, alignItems: "center" }} loader={this.spinner} />
                                    <p style={{margin: 0, padding: 0, color: "#352B48", fontWeight: "bold"}}> Jalur Merah </p>
                                </div>
                                <div style={{display: "flex", gap: 8, alignItems: "center"}}> 
                                    <Img alt="" src={require(`../../assets/infoKampus/HalteBus.svg`)} style={{ width: 50, height: 25, alignItems: "center" }} loader={this.spinner} />
                                    <p style={{margin: 0, padding: 0, color: "#352B48", fontWeight: "bold"}}> Halte Bus </p>
                                </div>
                                <div style={{display: "flex", gap: 8, alignItems: "center"}}> 
                                    <Img alt="" src={require(`../../assets/infoKampus/ArahBus.svg`)} style={{ width: 50, height: 25, alignItems: "center" }} loader={this.spinner} />
                                    <p style={{margin: 0, padding: 0, color: "#352B48", fontWeight: "bold"}}> Arah Bus </p>
                                </div>
                            </div>
                        </div>
                    )}
                    {(this.state.slide === "tempat" || this.state.slide === "transportasi") && (
                        <Img alt = "legenda" src = { require("../../assets/infoKampus/legenda.png" )} className = "w-80-only" style={{overflow: "hidden", borderRadius: 6}} loader={this.spinner}/>
                    )}
                </div>
            </div>
        )
    }
}

export const Chips = ( { active, cb, children } ) => (
    <div style={{cursor: 'pointer', borderRadius: 30, border: !active ? "2px solid #FB6700" : "0px solid #fff", width: "fit-content", backgroundColor: active ? "#FFE4D1" : ""}} onClick={() => {
        cb()
    }}>
        <p style={{color: "#FB6700", padding: "6px 12px", margin: 0,}}> {children} </p>
    </div>
)

