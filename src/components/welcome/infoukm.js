import React from 'react'

import { Img } from 'react-image'

export default function InfoUKM() {
    return (
        <div style={{width: "100%", backgroundColor: "#FFFEF7", borderRadius: 4, gap: 32, padding: 32, alignItems: "center"}} className = "flex-tight flex-center" id = "info-ukm">
            <Img alt="info-ukm" src={require(`../../assets/welcomePage/UKM.svg`)} className = "w-35" />
            <div style={{display: "flex", flexDirection: "column", gap: 8}} className = "w-65">
                <h2 style={{color: "#352B48"}} className = "real-h2"> Info UKM </h2>
                <p style={{fontSize: 20, margin: 0, color: "#534764"}}>
                    Kamu ingin ada tambahan kegiatan di kampus?
                    <br />
                    Mirip seperti di SMA yang dinamakan ekstrakurikuler, kamu bisa meningkatkan minat dan bakat kamu melalui UKM ini.
                    <br />
                    <br />
                    Terdapat hingga 39 UKM yang terbuka dan kamu bisa ambil sepuasnya. Ada UKM jenis penalaran, olahraga, seni, keagamaan, E-Sport, Riset dan Teknologi, dan Keilmuan dan Pascakampus. Tapi jangan dilupain ya akademisnya!
                </p>
                <div style={{display: "flex", gap: 12}}>
                    <Button href = "https://bem.ui.ac.id/uikipedia/ukm/"> Akses UIPedia/UKM </Button>
                </div>
                <p style={{fontSize: 20, margin: 0, color: "#534764"}}> Karena UKM itu tingkat Universitas, mungkin kamu melihatnya itu terlalu besar cakupannya. Kamu juga bisa ambil yang tingkat fakultas saja seperti UKF dan UKOR. Untuk melihatnya, kamu bisa cek ke bagian Info Fasilkom di iMaba ini. </p>
            </div>
        </div>
    )
}

const Button = ({ href, children, outline = false }) => (
    <a href = {href} target='_blank' rel="noopener noreferrer" style={{marginTop: 16, marginBottom: 16, padding: 0, }}>
        <button style={{backgroundColor: !outline ? "#FB6700" : "#FFFEF7", borderRadius: 6, padding: "0.5em 1.25em", border: outline ? "3px solid #FB6700" : "3px solid #FB6700", color: !outline ? "#FDF7FF" : "#FB6700", fontWeight: "bold"}}>
            {children}
        </button>
    </a>
)