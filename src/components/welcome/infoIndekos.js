import React, { Component } from 'react'

import { Img } from 'react-image'
import Styles from '../../styles/infoKampus'
import Spinner from 'react-bootstrap/Spinner'
import Card from 'react-bootstrap/Card'
import CardDeck from 'react-bootstrap/CardDeck'
import Carousel from 'react-bootstrap/Carousel'

import data from '../../data/kampus-kost.json'

export default class InfoIndekos extends Component {  
    kost = data.kost

    spinner = <Spinner variant="warning" animation="border" />

    render() {
        return (
            <Styles>
                <div style={{display: "flex", flexDirection: "column", alignItems: "center", width: "100%", borderRadius: 4, gap: 16, padding: 40, backgroundColor : "#FFFEF7"}} id = "indekos">
                    <div style={{display: "flex", flexDirection: "column", gap: 8, alignItems: "center"}}>
                        <h2 className = "real-h2" style={{color: "#352B48", margin: 0, padding: 0,}}> Info Indekos </h2>
                    </div>
                    <Carousel controls indicators className="carousel-lg">
                        <Carousel.Item >
                            <CardDeck className="equal-height-deck">
                                { this.kost.slice(0,2).map( place => (
                                    <FlipCard 
                                        key={place.name}
                                        front={<Img className = "w-tempat-hidup" alt={place.name} src={require(`../../assets/infoKampus/${place.name}-map.png`)} loader={this.spinner} />}
                                        back={ <div><p>{place.desc}</p><p>Range biaya: {place.price}</p></div> } 
                                        footer={place.loc} 
                                    />
                                ))}                            
                            </CardDeck>
                        </Carousel.Item>
                        <Carousel.Item>
                            <CardDeck className="equal-height-deck">
                                { this.kost.slice(2,4).map( place => (
                                    <FlipCard 
                                    key={place.name}
                                    front={<Img className = "w-tempat-hidup" alt={place.name} src={require(`../../assets/infoKampus/${place.name}-map.png`)} loader={this.spinner} />}
                                    back={ <div><p>{place.desc}</p><p>Range biaya: {place.price}</p></div> } 
                                    footer={place.loc} />
                                ))}                            
                            </CardDeck>
                        </Carousel.Item>
                        <Carousel.Item>
                            <CardDeck className="equal-height-deck">
                                { this.kost.slice(4,6).map( place => (
                                    <FlipCard 
                                    key={place.name}
                                    front={<Img className = "w-tempat-hidup" alt={place.name} src={require(`../../assets/infoKampus/${place.name}-map.png`)} loader={this.spinner} />}
                                    back={ <div><p>{place.desc}</p><p>Range biaya: {place.price}</p></div> } 
                                    footer={place.loc} />
                                ))}                            
                            </CardDeck>
                        </Carousel.Item>
                    </Carousel>
                    <Carousel controls indicators className="carousel-md">
                        <Carousel.Item>
                            <CardDeck className="equal-height-deck">
                                { this.kost.slice(0,2).map( place => (
                                    <FlipCard 
                                    key={place.name}
                                    front={<Img className = "w-tempat-hidup" alt={place.name} src={require(`../../assets/infoKampus/${place.name}-map.png`)} loader={this.spinner} />}
                                    back={ <div><p>{place.desc}</p><p>Range biaya: {place.price}</p></div> } 
                                    footer={place.loc} />
                                ))}                            
                            </CardDeck>
                        </Carousel.Item>
                        <Carousel.Item>
                            <CardDeck className="equal-height-deck">
                                { this.kost.slice(2,4).map( place => (
                                    <FlipCard 
                                    key={place.name}
                                    front={<Img className = "w-tempat-hidup" alt={place.name} src={require(`../../assets/infoKampus/${place.name}-map.png`)} loader={this.spinner} />}
                                    back={ <div><p>{place.desc}</p><p>Range biaya: {place.price}</p></div> } 
                                    footer={place.loc} />
                                ))}                            
                            </CardDeck>
                        </Carousel.Item>
                        <Carousel.Item>
                            <CardDeck className="equal-height-deck">
                                { this.kost.slice(4).map( place => (
                                    <FlipCard 
                                    key={place.name}
                                    front={<Img className = "w-tempat-hidup" alt={place.name} src={require(`../../assets/infoKampus/${place.name}-map.png`)} loader={this.spinner} />}
                                    back={ <div><p>{place.desc}</p><p>Range biaya: {place.price}</p></div> } 
                                    footer={place.loc} />
                                ))}                            
                            </CardDeck>
                        </Carousel.Item>   
                    </Carousel>

                    <Carousel indicators controls={false} className="carousel-sm">
                        { this.kost.map( place => (
                            <Carousel.Item key={place.name}>
                                <FlipCard                             
                                front={<Img className = "w-tempat-hidup" alt={place.name} src={require(`../../assets/infoKampus/${place.name}-map.png`)} loader={this.spinner} />}
                                back={ <div><p>{place.desc}</p><p>Range biaya: {place.price}</p></div> } 
                                footer={place.loc} />
                            </Carousel.Item>
                        ))}                           
                    </Carousel>
                </div>
            </Styles>
        )
    }
}

const FlipCard = (props) => {
    return (
        <Card className="glue" >
            <Card className="subcard">
                {props.front}
                <Card.Body style={{margin: 0, padding: 0, color: "#352B48", textAlign: "center", fontWeight: "bold", fontSize: 24}}>
                    {props.footer}
                </Card.Body>

                <Card.Body style={{margin: 0, padding: 0, paddingBottom: 24, color: "#534764", textAlign: "justify"}}>
                    {props.back}
                </Card.Body>
            </Card>
        </Card>
    )
}


