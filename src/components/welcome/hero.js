import React from "react"
import { Img } from 'react-image'
import { Link } from 'react-router-dom'

const directory = [
    { title: "Info Kampus", imgName: "InfoKampus.svg", to: "/kampus", class: "kampus" },
    { title: "Info Fasilkom", imgName: "InfoFasilkom.svg", to: "/pacil/", class: "akademik" },
]


export default function hero() {
    return (
        <div style={{ minHeight: "65vh", backgroundColor: "#FFFEF7", width: "100%", display: "flex", justifyContent: "center", alignItems: "center", flexDirection: "column", height: "100%" , padding: 12}} className="hero-pacil">
            <Img alt="" className="image-hero" src={require("../../assets/welcomePage/Left.svg")} style={{ width: "fit-content", position: "absolute", left: 0, top: 120}} />
            <Img alt="" className="image-hero" src={require("../../assets/welcomePage/Right.svg")} style={{ width: "fit-content", position: "absolute", right: 0, top: 120}} />

            <div style={{ display: "flex", flexDirection: "column", alignItems: "center", gap: 8, zIndex: 50, paddingTop: 24,  paddingBottom: 24 }}>
                <div style={{ display: "flex", alignItems: "flex-end", gap: 4 }}>
                    <Img alt="" src={require("../../assets/iconlogo/ImabaText.svg")} style={{ width: "fit-content" }} />
                </div>
                <p style={{ maxWidth: 400, textAlign: "center", color: "#352B48" }}>
                    iMaba hadir untuk memberikan informasi-informasi penting dan menarik seputar akademik, kampus UI, Fasilkom, serta kanal informasi yang dapat kamu hubungi.
                </p>
            </div>
            <div style={{ display: "flex", gap: 16, justifyContent: "center", flexWrap: "wrap", zIndex: 50 }}>
                {directory.map((item, key) => (
                    <Link to={key === 1 ? item.to : ""} style={{ display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center", gap: 6, textDecoration: "none", margin: 0, padding:0 }} onClick = {() => {
                        if(key === 0) {
                            const element = document.getElementById("faculty-program");
                            const headerOffset = 100;
                            const elementPosition = element.getBoundingClientRect().top;
                            const offsetPosition = elementPosition + window.scrollY - headerOffset;
                            
                            window.scrollTo({
                                top: offsetPosition,
                                behavior: "smooth"
                            })
                        }
                    }} key={key} className = "p-hover-man">
                        <Img alt={item.class} src={require(`../../assets/welcomePage/${item.imgName}`)} className="card-animation" style={{ width: 200 }} />
                        <p style={{margin: 0, padding: 0, fontWeight: "normal"}}> {item.title} </p>
                    </Link>
                ))}
            </div>
            <Img alt={"arrow"} className = "image-hover" src={require(`../../assets/infoAkademik/arrow-circle-left.svg`)} style={{ width: 35, rotate: "-90deg", marginTop: 24, cursor: 'pointer' }} onClick={() => {
                const element = document.getElementById("faculty-program");
                const headerOffset = 100;
                const elementPosition = element.getBoundingClientRect().top;
                const offsetPosition = elementPosition + window.scrollY - headerOffset;
                
                window.scrollTo({
                    top: offsetPosition,
                    behavior: "smooth"
                })
            }}/>
        </div>
    )
}