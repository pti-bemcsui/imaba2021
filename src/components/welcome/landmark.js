import React, { Component } from 'react'

import { Img } from 'react-image'
import Carousel from 'react-bootstrap/Carousel'
import Styles from '../../styles/infoKampus'
import Spinner from 'react-bootstrap/Spinner'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

import data from '../../data/kampus-landmark.json'
export default class Landmark extends Component {  
    landmarks = data.landmarks  
    spinner = <Spinner variant="warning" animation="border" />

    render() {
        return (
            <Styles>
                <div style={{display: "flex", flexDirection: "column", alignItems: "center", width: "100%", backgroundColor: "#FFFEF7", borderRadius: 4, gap: 32, padding: 40}} id = 'landmark-ui'>
                    <div style={{display: "flex", flexDirection: "column", gap: 8, alignItems: "center"}}>
                        <h2 className = "bigger-h2" style={{color: "#352B48", margin: 0, padding: 0,}}> Landmark </h2>
                        <h2 style={{margin: 0, padding: 0,}} className = "real-h2 main-span" > Universitas Indonesia </h2>
                    </div>
                    <Carousel controls indicators>
                        { this.landmarks.map( landmark => (
                            <Carousel.Item key={landmark.name}>
                                <Row className="d-flex justify-content-center mw-80 p-responsive" style={{display: "flex", justifyContent: "center", margin: "auto", border: "2px solid #EEEEDF", borderRadius: 4, width: "100%", minHeight: 325}}>
                                    <Col lg = {5}>
                                        <Img alt="" src={ require(`../../assets/infoKampus/landmark/${landmark.name}.jpg`)} style={{width:"100%", height: "100%"}} loader={this.spinner} />
                                    </Col>
                                    <Col lg = {7}>
                                        <div className="subcard" style={{position: "relative", display: "flex", flexDirection: "column", 
                                        alignItems: "end", justifyContent: "space-between", height: "100%"}}>
                                            <div style={{display: "flex", flexDirection: "column"}}>
                                                <h3 style={{color: "#352B48", fontWeight: "bold"}}>{landmark.title}</h3>
                                                <p style={{color: "#534764"}}>{landmark.desc} </p>
                                            </div>
                                            <Chips> {landmark.credits} </Chips>
                                        </div>
                                    </Col>
                                </Row>
                            </Carousel.Item>
                        ))}                
                    </Carousel>
                </div>
            </Styles>
        )
    }
}

const Chips = ( { children } ) => (
    <div style={{borderRadius: 30, border: "2px solid #FB6700", width: "fit-content", display: "flex", flexDirection: "column", alignItems: "end"}}>
        <p style={{color: "#FB6700", padding: "6px 12px", margin: 0,}}> {children} </p>
    </div>
)

