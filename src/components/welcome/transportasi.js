import React, { Component } from 'react'

import { Img } from 'react-image'
import Card from 'react-bootstrap/Card'
import CardDeck from 'react-bootstrap/CardDeck'
import Carousel from 'react-bootstrap/Carousel'
import Spinner from 'react-bootstrap/Spinner'

import Styles from "../../styles/infoAkademik"

import data from '../../data/kampus-transport.json'

export default class Transportasi extends Component {
    transport = data.transport
    state = { term: 1 }
    spinner = <Spinner variant="warning" animation="border" />

    matkulCard = (name, title, desc) => {
        return (
            <Card key={name} className="subcard" style={{width: "100%"}}>
                <div className="header" style={{display: "flex", flexDirection: "column"}}>
                    <Img alt="" src={ require(`../../assets/welcomePage/${name}.svg`)} style={{width: 175, height: 175, margin: "auto", paddingBottom: 8}} loader={this.spinner}/>
                    <h3 style={{fontWeight: "bold", color: "#352B48", marign: 0, padding: 0}}>{title}</h3>              
                </div>
                <p>{desc}</p>
            </Card>
        )
    }

    displayCarousel = (transport) => {
        return (
            <div>
                <Carousel controls indicators className="carousel-lg">
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            { transport.slice(0,2).map( item => this.matkulCard(item.name, item.title, item.desc))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            { transport.slice(2,4).map( item => this.matkulCard(item.name, item.title, item.desc))}
                        </CardDeck>
                    </Carousel.Item>
                </Carousel>
                <Carousel controls={false} indicators className="carousel-md">
                <Carousel.Item>
                        <CardDeck className="my-3">
                            { transport.slice(0,2).map( item => this.matkulCard(item.name, item.title, item.desc))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            { transport.slice(2,4).map( item => this.matkulCard(item.name, item.title, item.desc))}
                        </CardDeck>
                    </Carousel.Item>
                </Carousel>
                <Carousel controls={false} indicators className="carousel-sm">
                    { transport.map( item => (
                        <Carousel.Item key={item.title}>
                            {this.matkulCard(item.name, item.title, item.desc)}
                        </Carousel.Item>
                    ))}
                </Carousel>
            </div>
        )
    }

    displayTerm = () => {
        const { transport } = data
        return this.displayCarousel(transport)
    }

    render() {
        return (
            <Styles>
                <div style={{width: "100%", backgroundColor: "#FFFEF7", borderRadius: 4, gap: 32, padding: 32, alignItems: "center", display: "flex", flexDirection: "column"}} id = "info-transportasi">
                    <div style={{display: "flex", gap: 32, width: "100%"}}>
                        <div style={{flexDirection: "column", border: "1px solid #E5DFEE", padding: 16, gap: 4, height: "100%", borderRadius: 4}} className = "w-50-new flex-lg-hidden">
                            <Img alt={"bikun"} src={require(`../../assets/welcomePage/Bikun.svg`)} style={{ width: "100%", height: "100%", margin: "auto" }} loader={this.spinner}/>
                            <h3 style={{fontWeight: "bold", color: "#352B48" }}> Bis Kuning </h3>
                            <p style={{fontWeight: "lighter", color: "#534764", textAlign: "justify"}}> Kamu bisa menuju Fasilkom dengan menggunakan bis kuning. Haltenya berada di beberapa titik yang bisa dilihat di peta transportasi. Tiap halte memiliki pasangan di sebrangnya kecuali halte FH dan Masjid UI karena letaknya sudah berdekatan. </p>
                        </div>
                        <div className = "flex-tight-new flex-center-new w-50-new" style={{gap: 32, alignItems: "center"}}>
                            <div style={{display: "flex", flexDirection: "column", gap: 8, width: "100%"}}>
                                <div style={{display: "flex", textAlign: "right", justifyContent: "end"}}>
                                    <Img alt="" src={ require(`../../assets/welcomePage/Transportasi.svg`)} style={{width:100, height: 100, display: "flex", flexDirection: "column", alignItems: ""}} loader={this.spinner}/>
                                </div>
                                <h2 style={{color: "#352B48", padding: 0, margin: 0}} className = "bigger-h2 h2-center-new"> Transportasi </h2>
                                <h2 style={{color: "#352B48", padding: 0, margin: 0}} className = "real-h2 main-span h2-center-new">di Universitas Indonesia </h2>
                            </div>
                        </div>
                    </div>
                    <div className = "grid-normal grid-lg-hidden" style={{gap: 16,}}>
                        {this.transport.slice(1).map((item, key) => (
                            <div style={{display: "flex", flexDirection: "column", border: "1px solid #E5DFEE", padding: 16, gap: 4, height: "100%", borderRadius: 4}} key={key}>
                                <Img alt={item.title} src={require(`../../assets/welcomePage/${item.name}.svg`)} style={{ width: 200, height: 200, margin: "auto" }} loader={this.spinner}/>
                                <h3 style={{fontWeight: "bold", color: "#352B48" }} > {item.title} </h3>
                                <p style={{fontWeight: "lighter", color: "#534764", textAlign: "justify"}}> { item.desc } </p>
                            </div>
                        ))}
                    </div>
                    {this.displayTerm()}
                </div>
            </Styles>
        )
    }
}
