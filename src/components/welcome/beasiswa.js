import React from 'react'

import { Img } from 'react-image'

export default function Beasiswa() {
    return (
        <div style={{width: "100%", backgroundColor: "#FFFEF7", borderRadius: 4, gap: 32, padding: 32, alignItems: "center"}} className = "flex-tight flex-center" id = "beasiswa">
            <div style={{display: "flex", flexDirection: "column", gap: 8}} className = "w-65">
                <h2 style={{color: "#352B48"}} className = "real-h2"> Beasiswa </h2>
                <p style={{fontSize: 20, margin: 0, color: "#534764"}}> 
                    Siapa sih yang tidak mau mendapat beasiswa?
                    <br />
                    Di UI, kita menerima banyak sekali beasiswa dari berbagai instansi. Beasiswa yang ditawarkan juga sangat beragam loh!
                    <br />
                    <br />
                    Dengan website Beasiswa UI, kamu dapat mengetahui beasiswa apa saja yang sedang dibuka. Setiap beasiswa akan mempunyai syarat-syarat tertentu jika kamu ingin melamar. </p>
                <div style={{display: "flex", gap: 12}}>
                    <Button href = "http://beasiswa.ui.ac.id"> Akses Beasiswa UI </Button>
                </div>
                <p style={{fontSize: 20, margin: 0, color: "#534764"}}> Beasiswa yang tersedia akan terus diupdate, jadi yuk dipantau terus websitenya! Siapa tahu ada beasiswa yang cocok buat kamu! </p>
            </div>
            <Img alt="beasiswa" src={require(`../../assets/welcomePage/Beasiswa.svg`)} className = "w-35" />
        </div>
    )
}

const Button = ({ href, children, outline = false }) => (
    <a href = {href} target='_blank' rel="noopener noreferrer" style={{marginTop: 16, marginBottom: 16, padding: 0, }}>
        <button style={{backgroundColor: !outline ? "#FB6700" : "#FFFEF7", borderRadius: 6, padding: "0.5em 1.25em", border: outline ? "3px solid #FB6700" : "3px solid #FB6700", color: !outline ? "#FDF7FF" : "#FB6700", fontWeight: "bold"}}>
            {children}
        </button>
    </a>
)