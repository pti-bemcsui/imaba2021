import React,  { useState  } from 'react'
import data from '../../data/kampus-kost.json'
import { Img } from 'react-image'
import { Accordion, Card } from 'react-bootstrap';

export default function BiayaHidup() {
    const biaya = data.biaya
    
    const [openItemIndex, setOpenItemIndex] = useState(0);

    const handleToggle = (key) => {
        setOpenItemIndex((prevIndex) => (prevIndex === key ? -1 : key));
    };

    return (
        <div style={{width: "100%", backgroundColor: "#FFFEF7", borderRadius: 4, display: "flex", flexDirection: "column", gap: 8, padding: 32, alignItems: "center"}} id = "biaya-hidup">
            <div className = "flex-normal flex-center" style={{gap: 8}}>
                <div style={{display: "flex", flexDirection: "column", gap: 16}} className = "style-end">
                    <h2 style={{color: "#352B48"}} className = "real-h2"> Biaya Hidup  </h2>
                    <p style={{margin:0, padding: 0, color: "#534764"}} className = "real-small"> Untuk kamu yang memilih untuk tinggal di indekos, jangan lupa pertimbangkan juga biaya-biaya di bawah ini. </p>
                </div>
                <Img alt = "kurikulum" src = { require("../../assets/welcomePage/BiayaHidup.svg" )} className = "w-35" style={{overflow: "hidden"}}/>
            </div>
            <div className = "flex-normal flex-center" style={{width: "100%", gap: 16}}>
                <div style={{display: "flex", flexDirection: "column", gap: 12, width: "100%"}}>
                    <Accordion defaultActiveKey="0" style={{display: "flex", flexDirection: "column", width: "100%", gap: 16}}>
                        {biaya.map((item, key) => (
                            <Card key={key} style={{ border: "1px solid #E5DFEE", backgroundColor: 'transparent', borderRadius: 6}}>
                                <Accordion.Toggle
                                    as={Card.Header}
                                    variant="link"
                                    eventKey={key.toString()}
                                    onClick={() => handleToggle(key)}
                                    style={{ fontSize: "18px", fontWeight: "bold", color: "#352B48", textDecoration: "none", cursor: "pointer", display: "flex", alignItems: "center", gap: 8, justifyContent: "space-between" }}>
                                        <div style={{display: "flex", alignItems: "center", gap: 14}}>
                                            <p style={{margin:0, padding: "0.05em 0.65em", fontWeight: "bold", backgroundColor: "#EEEEDF", borderRadius: 3.84, color: "#534764"}}> { key + 1 } </p>
                                            <h3 style={{margin:0, padding: 0, fontWeight: "bold", fontSize: 18}}> Biaya {item.title} </h3>
                                        </div>
                                    <Img
                                        alt="Chevron"
                                        src={require("../../assets/iconlogo/chevron-down.svg")}
                                        style={{
                                            width: 20,
                                            height: 20,
                                            transition: "transform 500ms",
                                            transform: `rotate(${openItemIndex === key ? "-180" : "0"}deg)`,
                                        }}
                                        className="chevron-icon"
                                    />
                                </Accordion.Toggle>
                                <Accordion.Collapse eventKey={key.toString()}>
                                    <Card.Body style={{ fontSize: "16px", color: '#352B48' }}>{item.desc}</Card.Body>
                                </Accordion.Collapse>
                            </Card>
                        ))}
                    </Accordion>
                </div>
            </div>
        </div>
    )
}