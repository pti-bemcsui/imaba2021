import React from 'react'

import { Img } from 'react-image'
import Styles from '../../styles/infoKampus'
import Carousel from 'react-bootstrap/Carousel'
import { CardDeck } from 'react-bootstrap'

export default function fakultas() {
    const fakultas = ["FK", "FKG", "FF", "FKM", "FIK", "FMIPA", "FT", "Fasilkom", "FH", "FEB", "FIB", "FPsi", "FISIP", "FIA", "Vokasi"]

    const content = (makara) => (
        <div style={{ display: "flex", alignItems: "center", flexDirection: "column", justifyContent: "center", gap: 8 }} key = {makara}>
            <Img alt={makara} draggable="false" src={require(`../../assets/infoKampus/makara/${makara}.png`)} style={{ width: "100%" }} />
            <p style={{ color: "#352B48", margin: 0, padding: 0 }}>{makara}</p>
        </div>
    )

    const displayCarousel = () => {
        return (
            <>
                <Carousel controls={false} indicators className="carousel-sm" style={{ width: "100%" }}>
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            {fakultas.slice(0, 3).map(item =>
                                content(item))
                            }
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item >
                        <CardDeck className="my-3">
                            {fakultas.slice(3, 6).map(item =>
                                content(item))
                            }
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item >
                        <CardDeck className="my-3">
                            {fakultas.slice(6, 9).map(item =>
                                content(item))
                            }
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item >
                        <CardDeck className="my-3">
                            {fakultas.slice(9, 12).map(item =>
                                content(item))
                            }
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item >
                        <CardDeck className="my-3">
                            {fakultas.slice(12, 15).map(item =>
                                content(item))
                            }
                        </CardDeck>
                    </Carousel.Item>
                </Carousel>
                <div className="w-65 grid-faculty" style={{ gap: 16 }}>
                    {fakultas.map(item => (
                        content(item)
                    ))}
                </div>
            </>
        )
    }

    return (
        <Styles>
            <div style={{ width: "100%", backgroundColor: "#FFFEF7", borderRadius: 4, gap: 32, padding: 32, paddingTop: 32, paddingBottom: 32 }} className="flex-tight flex-center" id="faculty-program">
                <h2 style={{ color: "#352B48" }} className="w-35 real-h2 h2-center"> Fakultas dan Program di <span className="main-span"> Universitas Indonesia </span> </h2>
                {displayCarousel()}
            </div>
        </Styles>
    )
}