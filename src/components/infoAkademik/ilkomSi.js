import React from 'react'

import { Img } from 'react-image'

export default function ilkomSi() {
    return (
        <div style={{width: "100%", backgroundColor: "#FDF7FF", borderRadius: 4, display: "flex", flexDirection: "column", gap: 8, padding: 32, alignItems: "center"}} id = "difference">
            <h2 style={{color: "#352B48", textAlign: "center"}}> <span className = "text-gradient real-h2"> Ilmu Komputer </span> dan <span className = "text-gradient real-h2"> Sistem Informasi</span></h2>
            <div className = "flex-tight flex-tight-center" style={{gap: 8, width: "100%"}}>  
                <div style={{display: "flex", flexDirection: "column", border: "1px solid #E5DFEE", padding: 16, gap: 4, height: "100%", borderRadius: 4,}} className = "w-50">
                    <h3 style={{fontWeight: "bold", color: "#352B48", textAlign: "center" }}> Ilmu Komputer </h3>
                    <div className = "flex-tight flex-tight-center" style={{gap: 8}}>
                        <Img alt = "ilkom" src = { require("../../assets/infoAkademik/IlmuKomputer.svg" )} style={{width: "fit-content"}}/>
                        <p style={{fontWeight: "lighter", color: "#534764", textAlign: "justify", fontSize: 12}}> Jurusan Ilmu Komputer (Computer Science) mengkaji pemanfaatan Ilmu Komputer, antara lain desain algoritma dan penerapannya dalam pengembangan perangkat lunak hingga pemutakhiran aplikasi berbasis kecerdasan buatan, robotika, pengolahan informasi multimedia, keamanan informasi dan jaringan, serta pengolahan data besar. </p>
                    </div>
                    <p style={{color: "#534764", fontSize: 14}}> Prospek alumnus dapat menjadi Full Stack Developer, Android Developer, Software Engineer, Data Scientist, dll. </p>
                </div>
                <div style={{display: "flex", flexDirection: "column", border: "1px solid #E5DFEE", padding: 16, gap: 4, height: "100%", borderRadius: 4}} className = "w-50">
                    <h3 style={{fontWeight: "bold", color: "#352B48", textAlign: "center" }}> Sistem Informasi </h3>
                    <div className = "flex-tight flex-tight-center" style={{gap: 8}}>
                        <Img alt = "sisfor" src = { require("../../assets/infoAkademik/SistemInformasi.svg" )} style={{width: "fit-content"}}/>
                        <p style={{fontWeight: "lighter", color: "#534764", textAlign: "justify", fontSize: 12}}> Jurusan Sistem Informasi (Information Systems) mengkaji pengembangan, pemanfaatan, dan pengelolaan Sistem Informasi dalam suatu organisasi. Jurusan tersebut memadukan konsep komputer dengan bisnis dan manajemen dengan tujuan menjembatani kebutuhan bisnis dengan aplikasi yang dirancang. </p>
                    </div>
                    <p style={{color: "#534764", fontSize: 14}}> Prospek alumnus dapat menjadi Data Analyst, Database Administrator, UI/UX Designer, Graphic Designer, dll. </p>
                </div>
            </div>
        </div>
    )
}
