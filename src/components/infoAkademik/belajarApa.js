import React from 'react'

import { Img } from 'react-image'
import data from '../../data/akademik-belajarApa.json'
import Spinner from 'react-bootstrap/Spinner'

const subcards = data.belajar

export default function BelajarApa() {
    const spinner = <Spinner variant="warning" animation="border" />

    return (
        <div style={{width: "100%", backgroundColor: "#FDF7FF", borderRadius: 4, display: "flex", flexDirection: "column", gap: 8, padding: 32, alignItems: "center"}} id = "belajar">
            <h2 style={{color: "#352B48", textAlign: "center"}} className = "real-h2"> Belajar Apa <span className = "text-gradient real-span"> di Fasilkom? </span> </h2>
            <div className = "grid-normal" style={{gap: 8,}}>
                {subcards.map((item, key) => (
                    <div className={subcards.length % 2 !== 0 && key === subcards.length - 1 ? "grid-item-full" : ""} style={{display: "flex", flexDirection: "column", border: "1px solid #E5DFEE", padding: 16, gap: 4, height: "100%", borderRadius: 4}} key={key}>
                        <Img alt={item.title} src={require(`../../assets/infoAkademik/${item.asset}`)} style={{ width: 200, height: 200, margin: "auto" }} loader={spinner}/>
                        <h3 style={{fontWeight: "bold", color: "#352B48" }} > {item.title} </h3>
                        <p style={{fontWeight: "lighter", color: "#534764", textAlign: "justify"}}> { item.desc } </p>
                    </div>
                ))}
            </div>
        </div>
    )
}
