import React from "react"
import { Img } from 'react-image'

export default function hero() {
    return (
        <div style={{display: "flex", gap: 16, paddingTop: 120, backgroundColor: "#FDF7FF", justifyContent: "space-between", width: "100%", overflow: "hidden"}} className="hero-akademik akademik-hero">
            <div style={{display: "flex", flexDirection: "column", justifyContent: "center", gap: 8}} className = "p-8">
                <div style={{ display: "flex", alignItems: "flex-end", gap: 4 }}>
                    <Img alt="" src={require("../../assets/iconlogo/ImabaText.svg")} style={{ width: 160 }} />
                    <Img alt="" src={require("../../assets/iconlogo/PacilText.svg")} style={{ width: 80 }} />
                </div>
                <h1 style={{color: "#352B48", fontSize: 52}}> Info Akademik </h1>
                <p style={{margin: 0, color: "#534764"}}> Di halaman ini, kamu dapat mengetahui seperti apa pembelajaran di Fasilkom, mulai dari jurusan, mata kuliah, sampai kurikulum. Simak halaman ini dan persiapkan kehidupan kuliahmu! </p>
            </div>
            <Img alt="info-akademik" src={require("../../assets/infoAkademik/hero-akademik.svg")} className = "image-hero-akademik"/>
        </div>
    )
}