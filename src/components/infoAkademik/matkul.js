import React, { Component } from 'react'

import Badge from 'react-bootstrap/Badge'
import Card from 'react-bootstrap/Card'
import CardDeck from 'react-bootstrap/CardDeck'
import Carousel from 'react-bootstrap/Carousel'

import Styles from "../../styles/infoAkademik"

import data from '../../data/akademik-matkul.json'

export default class matkul extends Component {
    state = { term: 1 }

    matkulCard = (name, prodi, sks, desc) => {
        return (
            <Card key={name} className="subcard" style={{width: "100%"}}>
                <div className="header" style={{display: "flex", flexDirection: "column"}}>
                    <h3 style={{fontWeight: "bold", color: "#352B48"}}>{name}</h3>
                    <div className="badgewrapper" style={{gap: 8, display: "flex", flexWrap: "wrap"}}>
                        <Badge variant={prodi === "IK/SI" ? "success" : (prodi === "IK" ? "primary" : "secondary")}>{prodi}</Badge>
                        <Badge variant="success">{sks} SKS</Badge>
                    </div>                    
                </div>
                <p style={{color: "#534764"}}>{desc}</p>
            </Card>
        )
    }

    displayCarousel = (term) => {
        return (
            <div>
                <Carousel controls indicators className="carousel-xl">
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            { term.slice(0,3).map( item => this.matkulCard(item.name, item.prodi, item.sks, item.desc))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            { term.slice(3,6).map( item => this.matkulCard(item.name, item.prodi, item.sks, item.desc))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className={`my-3`}>
                            { term.slice(6, 9).map( item => this.matkulCard(item.name, item.prodi, item.sks, item.desc))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className={`my-3`}>
                            { term.slice(9, 12).map( item => this.matkulCard(item.name, item.prodi, item.sks, item.desc))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className={`my-3`}>
                            { term.slice(12, 15).map( item => this.matkulCard(item.name, item.prodi, item.sks, item.desc))}
                        </CardDeck>
                    </Carousel.Item>
                </Carousel>
                <Carousel controls indicators className="carousel-lg">
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            { term.slice(0,2).map( item => this.matkulCard(item.name, item.prodi, item.sks, item.desc))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            { term.slice(2,4).map( item => this.matkulCard(item.name, item.prodi, item.sks, item.desc))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            { term.slice(4,6).map( item => this.matkulCard(item.name, item.prodi, item.sks, item.desc))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className={`my-3`}>
                            { term.slice(6, 8).map( item => this.matkulCard(item.name, item.prodi, item.sks, item.desc))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className={`my-3`}>
                            { term.slice(8, 10).map( item => this.matkulCard(item.name, item.prodi, item.sks, item.desc))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className={`my-3`}>
                            { term.slice(10, 12).map( item => this.matkulCard(item.name, item.prodi, item.sks, item.desc))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className={`my-3`}>
                            { term.slice(12, 14).map( item => this.matkulCard(item.name, item.prodi, item.sks, item.desc))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className={`my-3`}>
                            { term.slice(14).map( item => this.matkulCard(item.name, item.prodi, item.sks, item.desc))}
                        </CardDeck>
                    </Carousel.Item>
                </Carousel>
                <Carousel controls={false} indicators className="carousel-md">
                <Carousel.Item>
                        <CardDeck className="my-3">
                            { term.slice(0,2).map( item => this.matkulCard(item.name, item.prodi, item.sks, item.desc))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            { term.slice(2,4).map( item => this.matkulCard(item.name, item.prodi, item.sks, item.desc))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            { term.slice(4,6).map( item => this.matkulCard(item.name, item.prodi, item.sks, item.desc))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className={`my-3`}>
                            { term.slice(6, 8).map( item => this.matkulCard(item.name, item.prodi, item.sks, item.desc))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className={`my-3`}>
                            { term.slice(8, 10).map( item => this.matkulCard(item.name, item.prodi, item.sks, item.desc))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className={`my-3`}>
                            { term.slice(10, 12).map( item => this.matkulCard(item.name, item.prodi, item.sks, item.desc))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className={`my-3`}>
                            { term.slice(12, 14).map( item => this.matkulCard(item.name, item.prodi, item.sks, item.desc))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className={`my-3`}>
                            { term.slice(14).map( item => this.matkulCard(item.name, item.prodi, item.sks, item.desc))}
                        </CardDeck>
                    </Carousel.Item>
                </Carousel>
                <Carousel controls={false} indicators className="carousel-sm">
                    { term.map( item => (
                        <Carousel.Item key={item.name}>
                            {this.matkulCard(item.name, item.prodi, item.sks, item.desc)}
                        </Carousel.Item>
                    ))}
                </Carousel>
            </div>
        )
    }

    displayTerm = () => {
        const { term1 } = data
        return this.displayCarousel(term1)
    }

    render() {
        return (
            <Styles>
                <div style={{width: "100%", backgroundColor: "#FDF7FF", borderRadius: 4, gap: 32, padding: 32, alignItems: "center"}} className = "flex-tight flex-center" id = "matkul">
                    <div style={{display: "flex", flexDirection: "column", gap: 8}}>
                        <h2 style={{color: "#352B48", textAlign: "center", padding: 0, margin: 0}} className = "real-h2"> Mata Kuliah </h2>
                        <h2 style={{color: "#352B48", textAlign: "center", padding: 0, margin: 0}} className = "real-h2 text-gradient"> Tahun Pertama di Fasilkom </h2>
                    {this.displayTerm()}
                    </div>
                </div>
            </Styles>
        )
    }
}
