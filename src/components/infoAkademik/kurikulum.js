import React, { Component } from 'react'

import { Img } from 'react-image'

export default class kurikulum extends Component {
    state = { slide: "ik"}
    
    render() {
        const ik = [
            "Kecerdasan Komputasional", 
            "Teknologi Perangkat Lunak",
            "Pengolahan Informasi",
            "Arsitektur dan Infrastruktur"
        ]

        const si = [
            "Tata Kelola SI/TI",
            "E-Bisnis",
            "Ekonomi Digital"
        ]

        return (
            <div style={{width: "100%", backgroundColor: "#FDF7FF", borderRadius: 4, gap: 32, padding: 32, alignItems: "center"}} className = "flex-tight flex-center" id = "kurikulum">
                <Img alt = "kurikulum" src = { require("../../assets/infoAkademik/Kurikulum.svg" )} className = "w-35" style={{overflow: "hidden"}}/>
                <div style={{display: "flex", flexDirection: "column", gap: 8}} className = "w-65">
                    <h2 style={{color: "#352B48"}} className = "real-h2"> Kurikulum </h2>
                    <p className = "text-gradient" style={{fontSize: 20}}> Fakultas Ilmu Komputer menawarkan berbagai mata kuliah terapan yang dapat digolongkan ke dalam beberapa peminatan </p>
                    <div style={{display: "flex", flexDirection: "column", gap: 12}}>
                        <div style={{display: "flex", gap: 8}} className = "flex-small">
                            <Chips active={this.state.slide === "ik"} cb={() => this.setState({slide:"ik"})}> Ilmu Komputer </Chips>
                            <Chips active={this.state.slide === "si"} cb={() => this.setState({slide:"si"})}> Sistem Informasi </Chips>
                        </div>
                        {this.state.slide === "ik" && ik.map( (bidang, index) => (
                            <p key={index} style={{width: "100%", border: "1px solid #E5DFEE", margin: 0, borderRadius: 4, padding: 8, color: "#534764"}} >Bidang Minat {bidang}</p>
                        ))}
                        {this.state.slide === "si" && si.map( (bidang, index) => (
                            <p key={index} style={{width: "100%", border: "1px solid #E5DFEE", margin: 0, borderRadius: 4, padding: 8, color: "#534764"}} >Bidang Minat {bidang}</p>
                            ))}
                    </div>
                    <p style={{margin: 0, color: "#534764"}}> Semua mata kuliah wajib UI, wajib rumpun, wajib fakultas, wajib program studi, pilihan bidang minat dan pilihan lain yang diambil harus disesuaikan sehingga jumlah minimum total SKS menjadi 144 sks. </p>
                </div>
            </div>
        )
    }
}

export const Chips = ( { active, cb, children } ) => (
    <div style={{cursor: 'pointer', borderRadius: 30, border: !active ? "2px solid #3271ED" : "0px solid #fff", width: "fit-content", backgroundColor: active ? "#EDE6F8" : ""}} onClick={() => {
        cb()
    }}>
        <p style={{color: "#3271ED", padding: "6px 12px", margin: 0,}}> {children} </p>
    </div>
)