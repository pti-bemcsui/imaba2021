import React from 'react'

import { Img } from 'react-image'

export default function scele() {
    return (
        <div style={{width: "100%", backgroundColor: "#FDF7FF", borderRadius: 4, gap: 32, padding: 32, alignItems: "center"}} className = "flex-tight flex-center" id = "scele">
            <div style={{display: "flex", flexDirection: "column", gap: 8}} className = "w-65">
                <h2 style={{color: "#352B48"}} className = "real-h2"> SCeLE </h2>
                <p style={{fontSize: 20, margin: 0, color: "#534764"}}> Kegiatan pembelajaran di Fasilkom menggunakan Student Centered E-Learning Environment atau disingkat SCeLE. SCeLE Fasilkom memberi kamu akses materi dari semua matkul Fasilkom (kecuali matkul wajib universitas), mendapatkan pengumuman, mengumpulkan tugas, mengerjakan kuis, hingga mengerjakan ujian online. </p>
                <div style={{display: "flex", gap: 12}}>
                    <Button href = "https://scele.cs.ui.ac.id/"> Akses SCeLE </Button>
                    <Button href = "https://scele.cs.ui.ac.id/static/tutorial/tutorial_mahasiswa.pdf" outline> Tutorial SCeLE </Button>
                </div>
                <p style={{margin: 0, color: "#534764"}}> SCeLE UI tidak digunakan dalam pembelajaran di luar Fasilkom, sebab untuk materi wajib universitas, yaitu MPKT-B dan MPKT-A, kamu dapat mengaksesnya secara langsung melalui <a href = "https://emas.ui.ac.id/" target='_blank' rel="noopener noreferrer"> https://emas.ui.ac.id/</a>.</p>
                <p style={{margin: 0, color: "#534764"}}> Tenang aja, kamu akan diajarkan cara penggunaan SCeLE saat masuk kuliah nanti kok! </p>
            </div>
            <Img alt="scele" src={require(`../../assets/infoAkademik/Scele.svg`)} className = "w-35" />
        </div>
    )
}

const Button = ({ href, children, outline = false }) => (
    <a href = {href} target='_blank' rel="noopener noreferrer" style={{marginTop: 16, marginBottom: 16, padding: 0, }}>
        <button style={{backgroundColor: !outline ? "#3271ED" : "#FDF7FF", borderRadius: 6, padding: "0.5em 1.25em", border: outline ? "3px solid #3271ED" : "3px solid #3271ED", color: !outline ? "#FDF7FF" : "#3271ED", fontWeight: "bold"}}>
            {children}
        </button>
    </a>
)