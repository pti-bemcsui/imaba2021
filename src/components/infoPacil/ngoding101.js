import React, { Component } from 'react'

import { Img } from 'react-image'
import Spinner from 'react-bootstrap/Spinner'

import data from '../../data/pacil-ngoding.json'

export default class ngoding101 extends Component {
    state = { slide: "tips" }
    spinner = <Spinner variant="warning" animation="border" />

    displaySlide() {
        const tips = data.tips
        const belajar = data.belajar
        const latihan = data.latihan
        if (this.state.slide==="tips") {
            return (
                <div style={{display: "flex", flexDirection: "column", gap: 16, }}>
                    <p style={{margin: 0, padding: 0, color: "#534764"}}> Pada tahun pertama di Fasilkom, kamu akan belajar bahasa pemrograman Python dan Java. Nahh, apa saja yaa, tips-tips untuk belajar ngoding? </p>
                    <div style={{display: "flex", flexDirection: "column", gap: 12 }}>
                        {tips.map((item, key) => <Accordion key = {key} id = {key} title = {item} />)}
                        <div style={{display: "flex", flexDirection: "column", gap: 16, width: "100%", padding: "12px 20px", border: "1px solid #E5DFEE", borderRadius: 6 }}>
                            <div style={{display: "flex", gap: 12, alignItems: "center"}}>
                                <Img alt="exclamationmark" src={require(`../../assets/infoPacil/ExclamationMark.svg`)} loader={this.spinner} />
                                <h3 style={{margin:0, padding: 0, fontSize: 18, color: "#352B48", fontWeight: "bold"}}> DDP0 </h3>
                            </div>
                            <p style={{margin: 0, padding: 0, color: "#352B48"}}> Buat kamu yang belum pernah ngoding sebelumnya atau ingin latihan ngoding, bisa banget nih daftar DDP0. Di DDP0 kamu akan belajar bersama kakak tingkat mengenai dasar-dasar dalam pemrograman. </p>
                        </div>
                    </div>
                </div>
            )
        } else {
            return (
                <div style={{display: "flex", flexDirection: "column", gap: 16, width: "100%", height: "100%"}}>
                    <div style={{ display: "flex", gap: 8, backgroundColor: "#E5DFEE", padding: "1em", alignItems: "center" }}>
                        <Img alt="" src={require(`../../assets/infoPacil/BookNgoding.svg`)} style={{ width: 30, height: 30, alignItems: "center" }} loader={this.spinner} />
                        <h3 style={{ color: "#534764", margin: 0, padding: 0, fontWeight: "bold", fontSize: 20 }}> Belajar Ngoding </h3>
                    </div>
                    <div style={{display: "flex", gap: 16, flexWrap: "wrap", alignItems: "center", justifyContent: "center"}}>
                        {belajar.map((item, key) => (
                            <a href = {item.to} target='_blank' rel="noopener noreferrer" style={{display: "flex", flexDirection: "column", gap: 16, alignItems: "center", height: "fit-content"}} key = {key} className = "grid-4-man">
                                <div style={{backgroundColor: "#101630", padding: "2em", borderRadius: 6, height: "100%", display: "flex", alignItems: "center"}}>
                                    <Img alt="" src={require(`../../assets/iconlogo/${item.name}.png`)} style={{ alignItems: "center" }} loader={this.spinner} />
                                </div>
                                <p style={{margin: 0, padding: 0, color: "#352B48", fontWeight: "bold"}}> {item.title} </p>
                            </a>
                        ))}
                    </div>
                    <div style={{ display: "flex", gap: 8, backgroundColor: "#E5DFEE", padding: "1em", alignItems: "center" }}>
                        <Img alt="" src={require(`../../assets/infoPacil/clipboard-check.svg`)} style={{ width: 30, height: 30, alignItems: "center" }} loader={this.spinner} />
                        <h3 style={{ color: "#534764", margin: 0, padding: 0, fontWeight: "bold", fontSize: 20 }}> Latihan Ngoding </h3>
                    </div>
                    <div style={{display: "flex", gap: 16, flexWrap: "wrap", alignItems: "center", justifyContent: "center"}}>
                        {latihan.map((item, key) => (
                            <a href = {item.to} target='_blank' rel="noopener noreferrer" style={{display: "flex", flexDirection: "column", gap: 16, alignItems: "center", height: "fit-content"}} key = {key} className = "grid-4-man">
                                <div style={{backgroundColor: "#101630", padding: "2em", borderRadius: 6, height: "100%", display: "flex", alignItems: "center"}}>
                                    <Img alt="" src={require(`../../assets/iconlogo/${item.name}.png`)} style={{ alignItems: "center" }} loader={this.spinner} />
                                </div>
                                <p style={{margin: 0, padding: 0, color: "#352B48", fontWeight: "bold"}}> {item.title} </p>
                            </a>
                        ))}
                    </div>
                </div>
            )
        }
    }

    render() {        
        return (
            <div style={{ width: "100%", backgroundColor: "#FDF7FF", borderRadius: 4, gap: 32, padding: 32, alignItems: "center", flexDirection: "column" }} className="flex-tight flex-center" id="ngoding101">
                <div className = "flex-center flex-tight" style={{gap: 16, alignItems: "center"}}>
                    <Img alt="ngoding101" src={require(`../../assets/infoPacil/Ngoding101.svg`)} loader={this.spinner} />
                    <div style={{display: "flex", gap: 16, flexDirection: "column"}}>
                        <h2 style={{ fontWeight: "bold" }} className="bigger-h2 h2-center color-default">  <span className = "real-h2"> Ngoding </span> <span className = "text-gradient"> 101 </span> </h2>
                        <div style={{ display: "flex", gap: 8 }} className="flex-small">
                            <InteractiveChips active={this.state.slide === "tips"} cb={() => this.setState({ slide: "tips" })}> Tips </InteractiveChips>
                            <InteractiveChips active={this.state.slide === "resources"} cb={() => this.setState({ slide: "resources" })}> Resources </InteractiveChips>
                        </div>
                    </div>
                </div> 
                {this.displaySlide()}               
            </div>
        )
    }
}

const InteractiveChips = ({ active, cb, children }) => (
    <div style={{ cursor: 'pointer', borderRadius: 30, border: !active ? "2px solid #3271ED" : "0px solid #fff", width: "fit-content", backgroundColor: active ? "#EDE6F8" : "" }} onClick={() => {
        cb()
    }}>
        <p style={{ color: "#3271ED", padding: "6px 12px", margin: 0, }}> {children} </p>
    </div>
)

const Accordion = ( { id, title } ) => (
    <div style={{display: "flex", gap: 12, alignItems: "center", width: "100%", padding: "12px 20px", borderRadius: 6, border: "1px solid #E5DFEE"}}>
        <p style={{margin:0, padding: "0.025em 0.5em", fontWeight: "bold", fontSize: 12, backgroundColor: "#E5DFEE", borderRadius: 3.84, color: "#534764"}}> { id + 1 } </p>
        <h3 style={{margin:0, padding: 0, fontWeight: 500, fontSize: 18, color: "#352B48"}}> {title} </h3>
    </div>
)