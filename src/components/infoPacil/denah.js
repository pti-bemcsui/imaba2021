import React, { Component } from 'react'

import data from '../../data/pacil-denah.json'
import next from '../../assets/infoAkademik/arrow-circle-right.svg'
import prev from '../../assets/infoAkademik/arrow-circle-left.svg'
import Styles from "../../styles/infoPacil"
import { InteractiveChips } from "./kepanitiaan"
export default class denah extends Component {
    state = { gedung: "lama", oldKey: 0, newKey: 0, oldLantai: 0, newLantai: 0 }

    displayDenah(idx) {
        const old = data.old[idx]

        if (Number(idx) === Number(2)) {
            const lantai = this.state.oldLantai
            return (
                <div style={{display: "flex", flexDirection: "column", gap: 10, width: "100%"}}>
                    <div className="navigasi d-flex align-items-center" style={{display: "flex", alignItems: "center"}}>  
                        { lantai > 0 ? 
                        <span onClick={() => this.setState({ oldLantai: parseInt(lantai-1) })}><img alt="prev" src={prev} style={{width: 35}}/></span> :
                        <span><img alt="prev" src={prev} style={{opacity: ".5", width: 35}} /></span>}                                             
                        <h3 style={{margin: 0, marginRight: 8, marginLeft: 8, padding:0, color: "#352B48", fontWeight: "bold"}}>
                            Lantai {this.state.oldLantai + parseInt(1)}<br/>
                            {old.title}
                        </h3>
                        { lantai < parseInt(old.lantai.length-1) ? 
                        <span onClick={() => this.setState({ oldLantai: parseInt(lantai+1) })}><img alt="next" src={next} style={{width: 35}}/></span> :
                        <span><img alt="next" src={next} style={{opacity: ".5", width: 35}} /></span>} 
                    </div>
                    <div style={{width: "100%", justifyContent: "space-between", gap: 20, alignItems: "center"}} className = "flex-tight flex-center">
                        <div className = "w-35"><img alt="denah" style = {{width: "100%", border: "1px solid #E5DFEE", padding: 16, borderRadius: 6}} src={require(`../../assets/infoPacil/denah/old-${old.lantai[lantai].key}.svg`)}/></div>
                        <div className = "w-65" style={{backgroundColor: "#E5DFEE", padding: "16px 20px"}}>
                            <h4 className="mt-2">Keterangan: </h4>
                            <ul style={{padding:0, paddingLeft: 20, paddingRight: 20, margin: 0,}}>{ old.lantai[lantai].desc.map( (i, idx) => (<li key={idx}>{i}</li>))}</ul>
                        </div>
                    </div>
                </div>
            )
        } else {
            return (
                <div style={{width: "100%", justifyContent: "space-between", gap: 20, alignItems: "center"}} className = "flex-tight flex-center">
                    <div className = "w-35"><img style = {{width: "100%", border: "1px solid #E5DFEE", padding: 16, borderRadius: 6}} alt="denah" src={require(`../../assets/infoPacil/denah/old-${old.key}.svg`)} /></div>
                    <div className = "w-65" style={{backgroundColor: "#E5DFEE", padding: "16px 20px"}}>
                        <h4 style={{color: "#352B48"}}>Keterangan: </h4>
                        { old.desc.map( (i, idx) => (
                            <div key={idx} style={{color: "#352B48"}}>
                                <b>{i.head}</b>
                                <ul style={{padding:0, paddingLeft: 20, paddingRight: 20, margin: 0,}}>{ i.body.map( (j, id) => <li key={id}>{j}</li> )}</ul>
                            </div>
                        ))}
                    </div>
                </div>
            )
        } 
    }

    displayGedung() {        
        const { old, baru } = data
        const newLantai = this.state.newLantai
        return (
            <div style={{display: "flex", flexDirection: "column", gap: 26}}>
                <div style={{border: "1px solid #E5DFEE", padding: "16px 20px", borderRadius: 6}}>
                    <div style={{display: "flex", flexDirection: "column", gap: 10}}>
                        <h2 style = {{textAlign: "center", color: "#352B48", margin: 0, padding:0}} className="real-span h2-center"> Gedung Lama <span className = "text-gradient"> Fasilkom </span></h2>
                        <div style={{display: "flex", gap: 8, justifyContent: "center", flexWrap: "wrap", alignItems: "center"}} className = "flex-small">
                            { old.map( (gedung, idx) => (<InteractiveChips active={this.state.oldKey === idx} key={idx} cb = {() => this.setState({
                                oldKey: idx, oldLantai: 0
                            })}> {gedung.title} </InteractiveChips>))}
                        </div>
                        <div style={{width: "100%", border: "1px solid #E5DFEE"}}></div>
                        {this.displayDenah(this.state.oldKey)}
                    </div>
                </div>
                <div style={{border: "1px solid #E5DFEE", padding: "16px 20px", borderRadius: 6}}>
                    <div style={{display: "flex", flexDirection: "column", gap: 10}}>
                        <h2 style = {{textAlign: "center", color: "#352B48", margin: 0, padding:0}} className="real-span h2-center">  Gedung Baru <span className = "text-gradient"> Fasilkom </span></h2>
                        <div style={{width: "100%", border: "1px solid #E5DFEE"}}></div>
                        <div className="navigasi d-flex align-items-center" style={{display: "flex", alignItems: "center"}}>
                            { newLantai > 0 ? 
                            <span onClick={() => this.setState({ newLantai: parseInt(newLantai-1) })}><img alt="prev" src={prev} style={{width: 35}}/></span> :
                            <span><img alt="prev" src={prev} style={{opacity: ".5", width: 35}} /></span>}
                            <h3 style={{margin: 0, marginRight: 8, marginLeft: 8, padding:0, color: "#352B48", fontWeight: "bold"}}>
                                Lantai {newLantai > 0 ? newLantai : "Dasar"}
                            </h3>
                            { newLantai < parseInt(baru.length-1) ? 
                            <span onClick={() => this.setState({ newLantai: parseInt(newLantai+1) })}><img alt="next" src={next} style={{width: 35}}/></span> :
                            <span><img alt="next" src={next} style={{opacity: ".5", width: 35}} /></span>} 
                        </div>
                        <div style={{width: "100%", justifyContent: "space-between", gap: 20, alignItems: "center"}} className = "flex-tight flex-center">
                            <div className = "w-35">
                                {Number(newLantai) === Number(0) ? 
                                <img alt="denah" style = {{width: "100%", border: "1px solid #E5DFEE", padding: 16, borderRadius: 6}} src={require(`../../assets/infoPacil/denah/new-${newLantai}.svg`)} /> : 
                                <img alt="denah" style = {{width: "100%", border: "1px solid #E5DFEE", padding: 16, borderRadius: 6}} src={require(`../../assets/infoPacil/denah/new-${newLantai}.svg`)} />}
                            </div>
                            <div className = "w-65" style={{backgroundColor: "#E5DFEE", padding: "16px 20px"}}>
                                <h4 style={{color: "#352B48"}}>Keterangan: </h4>
                                <ul style={{padding:0, paddingLeft: 20, paddingRight: 20, margin: 0,}}> { baru[newLantai].desc.map( (i, idx) => (<li key={idx}>{i}</li>))}</ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    render() {
        return (
            <Styles>
                <div style={{width: "100%", backgroundColor: "#FDF7FF", borderRadius: 4, gap: 26, padding: 32, alignItems: "center", flexDirection: "column"}} denah = "flex-tight flex-center" id = "denah">
                    <h2 className="text-center bigger-h2" style={{color: "#352B48"}}>Denah</h2>
                    {this.displayGedung()}
                </div>
            </Styles>
        )
    }
}
