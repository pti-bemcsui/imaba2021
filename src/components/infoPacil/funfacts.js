import React from 'react'

import Carousel from 'react-bootstrap/Carousel'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import { Img } from "react-image"

import data from '../../data/pacil-funfacts.json'
import Styles from "../../styles/infoPacil"
import Spinner from 'react-bootstrap/Spinner'

export default function funfacts() {
    const facts = data.funfacts
    const spinner = <Spinner variant="warning" animation="border" />

    return (
        <Styles>
            <div style={{width: "100%", backgroundColor: "#FDF7FF", borderRadius: 4, gap: 32, padding: 32, alignItems: "center", flexDirection: "column"}} className = "flex-tight flex-center" id = "funfacts">
                <div className = "flex-normal flex-center" style={{gap: 16, alignItems: "center"}}>
                    <div style={{display: "flex", gap: 12, flexDirection: "column"}} className = "w-65">
                        <h2 style = {{color: "#352B48"}} className="real-h2 h2-center"> <span className = "real-span text-gradient"> 7 </span> Fun Facts <span className = "real-span text-gradient"> Fasilkom </span></h2>
                        <p style={{margin: 0, padding: 0, color: "#534764"}}> Yuk simak fakta-fakta menarik yang ada di Fakultas Ilmu Komputer yang mungkin dapat membantu kamu selama berkuliah di Fasilkom. </p>
                    </div>
                    <Img alt = "funfact-panda" src = { require("../../assets/infoPacil/FunfactPanda.svg" )} className = "w-35" style={{overflow: "hidden"}}/>
                </div>
                <Carousel controls indicators >
                    { facts.map( fact => (
                        <Carousel.Item key={fact.name} className="fact">
                            <Row className="funfacts" style={{marginBottom: 12}}>
                                <Col xl={3} lg={4} md={4}>
                                    {fact.name === "cs" || fact.name === "kontingen" ? 
                                    <Img className = "img-try "alt="" src={require(`../../assets/iconlogo/${fact.name}.png`)} loader={spinner} />
                                    : <Img className = "img-try" alt="" src={require(`../../assets/infoPacil/funfacts-${fact.name}`)} loader={spinner} />}
                                </Col>
                                <Col xl={9} lg={8} md={8} className = "subcard">
                                    <h3 style={{color: "#352B48", fontWeight: "bold"}}>{fact.title}</h3>
                                    <p style={{color: "#534764"}}>{fact.desc}</p>
                                </Col>
                            </Row>
                        </Carousel.Item>
                    ))}                    
                </Carousel>
            </div>
        </Styles>
    )
}
