import React, { Component } from 'react'

import { Img } from 'react-image'
import Styles from "../../styles/infoPacil"
import data from '../../data/pacil-kepanitiaan.json'
import Spinner from 'react-bootstrap/Spinner'
import Card from 'react-bootstrap/Card'
import CardDeck from 'react-bootstrap/CardDeck'
import Carousel from 'react-bootstrap/Carousel'

export default class kepanitiaan extends Component {
    state = { slide: "bem" }
    spinner = <Spinner variant="warning" animation="border" />

    matkulCard = (name, title, desc, link) => {
        return (
            <Card key={name} className="subcard" style={{width: "100%"}}>
                <div className="header flex-small" style={{display: "flex", gap: 8, alignItems: "center"}}>
                    <Img alt="" src={ require(`../../assets/iconlogo/${name}.png`)} style={{width:100, height: 100, paddingBottom: 8, margin: 0, objectFit: "cover", borderRadius: "100%", }} loader={this.spinner}/>

                    <h3 style={{fontWeight: "bold", color: "#352B48", marign: 0, padding: 0}}>{title}</h3>
                </div>
                <p style={{color: "#534764", margin: 0}}> {desc} </p>
                <Chips imageIcon={<Img alt="" src={require(`../../assets/iconlogo/InstagramIcon.svg`)} style={{ width: 15, height: 15, }} loader={this.spinner} />} link = {link}> 
                    {title} 
                </Chips>            
            </Card>
        )
    }

    displayCarousel = (data, name) => {
        return (
            name === "bem" ? (<div>
                <Carousel controls indicators className="carousel-xl">
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            { data.slice(0,2).map( item => this.matkulCard(item.name, item.title, item.desc, item.link))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            { data.slice(2,4).map( item => this.matkulCard(item.name, item.title, item.desc, item.link))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            { data.slice(4,6).map( item => this.matkulCard(item.name, item.title, item.desc, item.link))}
                        </CardDeck>
                    </Carousel.Item>
                </Carousel>
                <Carousel controls indicators className="carousel-lg">
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            { data.slice(0,2).map( item => this.matkulCard(item.name, item.title, item.desc, item.link))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            { data.slice(2,4).map( item => this.matkulCard(item.name, item.title, item.desc, item.link))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            { data.slice(4,6).map( item => this.matkulCard(item.name, item.title, item.desc, item.link))}
                        </CardDeck>
                    </Carousel.Item>
                </Carousel>
                <Carousel controls={false} indicators className="carousel-md">
                    { data.map( item => (
                        <Carousel.Item key={item.title}>
                            {this.matkulCard(item.name, item.title, item.desc, item.link)}
                        </Carousel.Item>
                    ))}
                </Carousel>
                <Carousel controls={false} indicators className="carousel-sm">
                    { data.map( item => (
                        <Carousel.Item key={item.title}>
                            {this.matkulCard(item.name, item.title, item.desc, item.link)}
                        </Carousel.Item>
                    ))}
                </Carousel>
            </div>) : (
                <div className = "flex-normal flex-center" style={{gap: 32}}>
                    { data.map( item => this.matkulCard(item.name, item.title, item.desc, item.link))}
                </div>
            )
        )
    }

    displayTerm = (data, name) => {
        return this.displayCarousel(data, name)
    }

    displaySlide() {
        const bem = data.bem
        const dpm = data.dpm
        if (this.state.slide === "bem") {
            return this.displayTerm(bem, "bem")
        } else {
            return this.displayTerm(dpm, "dpm")
        }
    }

    render() {
        return (
            <Styles>
                <div style={{width: "100%", backgroundColor: "#FDF7FF", borderRadius: 4, gap: 16, padding: 32, alignItems: "center", flexDirection: "column"}} className = "flex-tight flex-center" id = "kepanitiaan">
                    <h2 style = {{color: "#352B48"}} className="real-h2 h2-center"> Kepanitiaan </h2>
                    <div style={{display: "flex", gap: 8}} className = "flex-small">
                        <InteractiveChips active={this.state.slide === "bem"} cb={() => this.setState({slide:"bem"})}> BEM Fasilkom UI </InteractiveChips>
                        <InteractiveChips active={this.state.slide === "dpm"} cb={() => this.setState({slide:"dpm"})}> DPM Fasilkom UI </InteractiveChips>
                    </div>
                    {this.displaySlide()}
                </div>
            </Styles>
        )
    }
}

const Chips = ({ imageIcon, link, children, }) => (
    <a href = {link} target='_blank' rel="noopener noreferrer">
        <div style={{ borderRadius: 30, border: "2px solid #3271ED", width: "fit-content", display: "flex", alignItems: "center", padding: "0px 12px", cursor: 'pointer'}}>
            {imageIcon}
            <p style={{ color: "#3271ED", padding: 0, margin: 0, borderLeft: "1px solid #3271ED", marginLeft: 6, paddingLeft: 6 }}> {children} </p>
        </div>
    </a>
)


export const InteractiveChips = ( { active, cb, children } ) => (
    <div style={{cursor: 'pointer', borderRadius: 30, border: !active ? "2px solid #3271ED" : "0px solid #fff", width: "fit-content", backgroundColor: active ? "#EDE6F8" : ""}} onClick={() => {
        cb()
    }}>
        <p style={{color: "#3271ED", padding: "6px 12px", margin: 0,}}> {children} </p>
    </div>
)