import React from 'react'

import { Img } from 'react-image'
import Spinner from 'react-bootstrap/Spinner'

import data from '../../data/pacil-life.json'

const spinner = <Spinner variant="warning" animation="border" />

export default function pacilLife() {
    const life = data.life
    return (
        <div style={{width: "100%", backgroundColor: "#FDF7FF", borderRadius: 4, gap: 32, padding: 32, display: "flex", flexDirection:"column"}} id = "pacilLife">
            <div className = "flex-tight" style={{gap: 32, alignItems: "center"}}>
                <Img alt="pacilLife" src={require(`../../assets/infoPacil/pacilLife.svg`)} />
                <div style={{display: "flex", flexDirection: "column", gap: 8}} className = "h2-center">
                    <h2 style={{color: "#352B48", padding: 0, margin: 0}} className = "real-h2"> Living in </h2>
                    <h2 style={{color: "#352B48", padding: 0, margin: 0}} className = "bigger-h2 text-gradient"> Fasilkom </h2>
                </div>
            </div>
            <div style={{gap: 16}} className = "grid-2-man">
                {life.map((item, index) => (
                    <Section title={item.title} desc={item.p} img={item.img} key={index} reverse={index % 2 !== 0} />
                ))}
            </div>
        </div>
    )
}

const Section = ({ title, desc, img, reverse = false }) => (
    <div style={{backgroundColor: "#E5DFEE", border: "1px solid #AFA8BA", borderRadius: 6, padding: "12px 20px", display: "flex", flexDirection: reverse ? "row-reverse" : "row", gap: 8, alignItems: "center", justifyContent: reverse ? "start" : "end"}} className = "flex-small-reverse">
        <div style={{display: "flex", flexDirection: "column", gap: 12}}>
            <h3 className = "real-small" style={{margin: 0, padding: 0, fontWeight: 'bold', color: "#352B48", fontSize: 16}}> {title} </h3>
            <p style={{margin: 0, padding: 0, color: "#000", fontSize: 14}}> {desc} </p>
        </div>
        <Img alt="pacilLife" src={require(`../../assets/infoPacil/pacilLife/${img}.svg`)} style = {{width: 135, height: 135}} loader={spinner}/>
    </div>
)