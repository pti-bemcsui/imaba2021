import React from 'react'

import Card from 'react-bootstrap/Card'

import data from '../../data/pacil-yelyel.json'
import Styles from "../../styles/infoPacil"
import Carousel from 'react-bootstrap/Carousel'

export default function yelyel() {
    const yelyel = data.yelyel
   const yelyelCard = (title, lyrics) => {
        return (
            <Card key={title} className="subcard" style={{width: "100%", height: "100%"}}>
                <div className="header" style={{display: "flex", flexDirection: "column"}}>
                    <h3 style={{fontWeight: "bold", color: "#352B48", textAlign: "center"}}>{title}</h3>
                </div>
                <div className="lyrics d-flex flex-column justify-content-center">
                    {lyrics.map( (line, idx) => (
                        <span key={idx}>{line}</span>
                    ))}                            
                </div>
            </Card>
        )
    }

    const displayCarousel = (yelyel) => {
        return (
            <div className = "flex-normal flex-center">
                <Carousel controls={false} indicators className="carousel-sm" style={{width: "100%"}}>
                    { yelyel.map( item => (
                        <Carousel.Item key={item.title} style={{width: "100%"}}>
                            {yelyelCard(item.title, item.lyrics)}
                        </Carousel.Item>
                    ))}
                </Carousel>
                <div style={{gap: 16, alignItems: "center"}} className = "grid-normal flex-md-hidden">
                    { yelyel.map( item => (
                        yelyelCard(item.title, item.lyrics)
                    ))}
                </div>
            </div>
        )
    }

    const displayTerm = () => {
        return displayCarousel(yelyel)
    }

    return (
        <Styles>
            <div style={{width: "100%", backgroundColor: "#FDF7FF", borderRadius: 4, gap: 32, padding: 32, alignItems: "center", flexDirection: "column"}} className = "flex-tight flex-center" id = "yelyel">
                <div style={{display: "flex", flexDirection: "column", gap: 8}}>
                    <h2 style={{color: "#352B48", textAlign: "center", padding: 0, margin: 0}} className = "bigger-h2"> Mars dan Yel-yel </h2>
                    <h2 style={{color: "#352B48", textAlign: "center", padding: 0, margin: 0}} className = "real-h2 text-gradient"> Fasilkom UI </h2>
                {displayTerm()}
                </div>
                <div class="video-container">
                    <embed type="video/webm" src="https://res.cloudinary.com/di5jtzwui/video/upload/f_auto:video,q_auto/v1/PTI/ffpnhx0iyygclctoejwr" style={{overflow: 'hidden'}}/>
                </div>
            </div>
        </Styles>
    )
}
