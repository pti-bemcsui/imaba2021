import React from "react"
import { Img } from 'react-image'

export default function hero() {
    return (
        <div style={{display: "flex", gap: 16, paddingTop: 120, backgroundColor: "#FDF7FF", justifyContent: "space-between", width: "100%", overflow: "hidden"}} className="hero-akademik akademik-hero">
            <div style={{display: "flex", flexDirection: "column", justifyContent: "center", gap: 8}} className = "p-8">
                <div style={{ display: "flex", alignItems: "flex-end", gap: 4 }}>
                    <Img alt="" src={require("../../assets/iconlogo/ImabaText.svg")} style={{ width: 160 }} />
                    <Img alt="" src={require("../../assets/iconlogo/PacilText.svg")} style={{ width: 80 }} />
                </div>
                <h1 style={{color: "#352B48", fontSize: 52}}> Info Non-Akademik </h1>
                <p style = {{margin: 0, color: "#352B48"}}> Tak kenal maka tak sayang. Yuk, kenali Fasilkom lebih dalam! Halaman ini akan menjelaskan berbagai macam hal tentang fakultas kita tercinta. </p>
            </div>
            <Img alt="info-akademik" src={require("../../assets/infoPacil/hero.svg")} className = "image-hero-akademik"/>
        </div>
    )
}