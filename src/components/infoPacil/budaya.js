import React,  { useRef } from 'react'
import data from '../../data/pacil-budaya.json'
import { Img } from 'react-image'
import { Accordion, Card } from 'react-bootstrap';

export default function budaya() {
    const budayas = data.budaya

    const accordionItemRefs = budayas.map(() => useRef(null));

    const handleToggle = (key) => {
        const chevronIcon = accordionItemRefs[key].current.querySelector('.chevron-icon');
        const rotationDeg = chevronIcon.style.transform ? parseFloat(chevronIcon.style.transform.replace(/[^0-9.]/g, '')) : 0;
    
        const newRotationDeg = rotationDeg + 90; // Always add 180 to the current rotation
    
        chevronIcon.style.transform = `rotate(${newRotationDeg}deg)`;
        accordionItemRefs[key].current.click();
    };
    

    return (
        <div style={{width: "100%", backgroundColor: "#FDF7FF", borderRadius: 4, display: "flex", flexDirection: "column", gap: 8, padding: 32, alignItems: "center"}} id = "budaya">
            <h2 style={{color: "#352B48", textAlign: "center"}} className = "real-h2"> Budaya <span className = "text-gradient real-span"> di Fasilkom </span> </h2>
            <div className = "flex-normal flex-center" style={{width: "100%", gap: 16}}>
                <Img alt = "kurikulum" src = { require("../../assets/infoPacil/Budaya Fasilkom.svg" )} className = "w-35" style={{overflow: "hidden"}}/>
                <div style={{display: "flex", flexDirection: "column", gap: 12}} className = "w-65">
                    <Accordion defaultActiveKey="0" style={{display: "flex", flexDirection: "column", gap: 16}}>
                        {budayas.map((item, key) => (
                            <Card key={key} style={{ border: "1px solid #E5DFEE", backgroundColor: 'transparent', borderRadius: 6}}>
                                <Accordion.Toggle as={Card.Header} variant="link" eventKey={key.toString()} 
                                 ref={accordionItemRefs[key]} // Assign the ref
                                 onClick={() => handleToggle(key)}
                                 style={{ fontSize: "18px", fontWeight: "bold", color: "#352B48", textDecoration: "none", cursor: "pointer", display: "flex", alignItems: "center", gap: 8, justifyContent: "space-between" }}>
                                    <div style={{display: "flex", alignItems: "center", gap: 14}}>
                                        <p style={{margin:0, padding: "0.025em 0.5em", fontWeight: "bold", fontSize: 12, backgroundColor: "#E5DFEE", borderRadius: 3.84, color: "#534764"}}> { key + 1 } </p>
                                        <h3 style={{margin:0, padding: 0, fontWeight: "bold", fontSize: 18, color: "#352B48"}}> {item.title} </h3>
                                    </div>
                                    <Img alt = "Chevron" src = { require("../../assets/iconlogo/Chevron.svg" )} style={{width: 15, height: 15, transition: "transform 500ms", rotate: key === 0 ? "180deg": "0deg"}}
                                    className="chevron-icon"
                                    />

                                </Accordion.Toggle>
                                <Accordion.Collapse eventKey={key.toString()}>
                                    <Card.Body style={{ fontSize: "16px", color: '#352B48' }}>{item.p}</Card.Body>
                                </Accordion.Collapse>
                            </Card>
                        ))}
                    </Accordion>
                </div>
            </div>
        </div>
    )
}