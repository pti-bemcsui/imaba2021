import React, { Component } from 'react'

import { Img } from 'react-image'
import Styles from "../../styles/infoPacil"
import Spinner from 'react-bootstrap/Spinner'
import Card from 'react-bootstrap/Card'
import CardDeck from 'react-bootstrap/CardDeck'
import Carousel from 'react-bootstrap/Carousel'
import data from "../../data/Ukf.json"

export default class UKORdanUKF extends Component {
    state = { slide: "ukf" }
    spinner = <Spinner variant="warning" animation="border" />

    matkulCard = (name, title, desc, link, datatype) => {
        return (
            <Card key={title} className="subcard" style={{ width: "100%" }}>
                <div className="header flex-small" style={{ display: "flex", gap: 8, alignItems: "center" }}>
                    <Img alt="" src={require(`../../assets/iconlogo/${name}`)} style={{ width: 100, height: 100, paddingBottom: 8, margin: 0, objectFit: "contain", borderRadius: "100%" }} loader={this.spinner} />

                    <h3 style={{ fontWeight: "bold", color: "#352B48", marign: 0, padding: 0 }}>{title}</h3>
                </div>
                <p style={{ color: "#534764", margin: 0 }}> {desc} </p>
                {datatype === "ukf" && link.map((item, key) => <Chips imageIcon={<Img alt="" src={require(`../../assets/iconlogo/InstagramIcon.svg`)} style={{ width: 15, height: 15, }} loader={this.spinner} />} link={item.url} key = {key}>
                    {title}
                </Chips>)}
            </Card>
        )
    }

    displayCarousel = (data, name) => {
        return (
            <div>
                <Carousel controls indicators className="carousel-xl">
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            {data.slice(0, 2).map(item => this.matkulCard(item.img, item.title, item.desc, item.link, name))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            {data.slice(2, 4).map(item => this.matkulCard(item.img, item.title, item.desc, item.link, name))}
                        </CardDeck>
                    </Carousel.Item>
                    {name === "ukor" && <Carousel.Item>
                        <CardDeck className="my-3">
                            {data.slice(4, 6).map(item => this.matkulCard(item.img, item.title, item.desc, item.link, name))}
                        </CardDeck>
                    </Carousel.Item>}
                    {name === "ukor" && <Carousel.Item>
                        <CardDeck className="my-3">
                            {data.slice(6, 8).map(item => this.matkulCard(item.img, item.title, item.desc, item.link, name))}
                        </CardDeck>
                    </Carousel.Item>}
                    {name === "ukor" && <Carousel.Item>
                        <CardDeck className="my-3">
                            {data.slice(8, 10).map(item => this.matkulCard(item.img, item.title, item.desc, item.link, name))}
                        </CardDeck>
                    </Carousel.Item>}
                    {name === "ukor" && <Carousel.Item>
                        <CardDeck className="my-3">
                            {data.slice(10).map(item => this.matkulCard(item.img, item.title, item.desc, item.link, name))}
                        </CardDeck>
                    </Carousel.Item>}
                </Carousel>
                <Carousel controls indicators className="carousel-lg">
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            {data.slice(0, 2).map(item => this.matkulCard(item.img, item.title, item.desc, item.link, name))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            {data.slice(2, 4).map(item => this.matkulCard(item.img, item.title, item.desc, item.link, name))}
                        </CardDeck>
                    </Carousel.Item>
                    {name === "ukor" && <Carousel.Item>
                        <CardDeck className="my-3">
                            {data.slice(4, 6).map(item => this.matkulCard(item.img, item.title, item.desc, item.link, name))}
                        </CardDeck>
                    </Carousel.Item>}
                    {name === "ukor" && <Carousel.Item>
                        <CardDeck className="my-3">
                            {data.slice(6, 8).map(item => this.matkulCard(item.img, item.title, item.desc, item.link, name))}
                        </CardDeck>
                    </Carousel.Item>}
                    {name === "ukor" && <Carousel.Item>
                        <CardDeck className="my-3">
                            {data.slice(8, 10).map(item => this.matkulCard(item.img, item.title, item.desc, item.link, name))}
                        </CardDeck>
                    </Carousel.Item>}
                    {name === "ukor" && <Carousel.Item>
                        <CardDeck className="my-3">
                            {data.slice(10).map(item => this.matkulCard(item.img, item.title, item.desc, item.link, name))}
                        </CardDeck>
                    </Carousel.Item>}
                </Carousel>
                <Carousel controls={false} indicators className="carousel-md">
                    {data.map(item => (
                        <Carousel.Item key={item.title}>
                            {this.matkulCard(item.img, item.title, item.desc, item.link, name)}
                        </Carousel.Item>
                    ))}
                </Carousel>
                <Carousel controls={false} indicators className="carousel-sm">
                    {data.map(item => (
                        <Carousel.Item key={item.title}>
                            {this.matkulCard(item.img, item.title, item.desc, item.link, name)}
                        </Carousel.Item>
                    ))}
                </Carousel>
            </div>
        )
    }

    displayTerm = (data, name) => {
        return this.displayCarousel(data, name)
    }

    displaySlide() {
        const ukf = data.UKF
        const ukor = data.UKOR
        if (this.state.slide === "ukf") {
            return this.displayTerm(ukf, "ukf")
        } else {
            return this.displayTerm(ukor, "ukor")
        }
    }

    render() {
        return (
            <Styles>
                <div style={{ width: "100%", backgroundColor: "#FDF7FF", borderRadius: 4, gap: 32, padding: 32, alignItems: "center", flexDirection: "column" }} className="flex-tight flex-center" id="ukor-ukf">
                    <div className = "flex-center flex-tight" style={{gap: 16, alignItems: "center"}}>
                        <div style={{display: "flex", gap: 16, flexDirection: "column", alignItems: "center"}}>
                            <h2 style={{ fontWeight: "bold" }} className="real-h2 h2-center color-default"> <span className = "text-gradient"> UKF </span> <span className = "real-span"> dan </span> <span className = "text-gradient"> UKOR </span> </h2>
                            <div style={{ display: "flex", gap: 8 }} className="flex-small">
                                <InteractiveChips active={this.state.slide === "ukf"} cb={() => this.setState({ slide: "ukf" })}> Unit Kesenian Fakultas </InteractiveChips>
                                <InteractiveChips active={this.state.slide === "ukor"} cb={() => this.setState({ slide: "ukor" })}> Unit Kegiatan Olahraga </InteractiveChips>
                            </div>
                        </div>
                        <Img alt="" src={require(`../../assets/infoPacil/UKM.svg`)} loader={this.spinner} />
                    </div>
                    {this.displaySlide()}
                </div>
            </Styles>
        )
    }
}

const Chips = ({ imageIcon, link, children, }) => (
    <a href={link} target='_blank' rel="noopener noreferrer">
        <div style={{ borderRadius: 30, border: "2px solid #3271ED", width: "fit-content", display: "flex", alignItems: "center", padding: "0px 12px", cursor: 'pointer' }}>
            {imageIcon}
            <p style={{ color: "#3271ED", padding: 0, margin: 0, borderLeft: "1px solid #3271ED", marginLeft: 6, paddingLeft: 6 }}> {children} </p>
        </div>
    </a>
)


const InteractiveChips = ({ active, cb, children }) => (
    <div style={{ cursor: 'pointer', borderRadius: 30, border: !active ? "2px solid #3271ED" : "0px solid #fff", width: "fit-content", backgroundColor: active ? "#EDE6F8" : "" }} onClick={() => {
        cb()
    }}>
        <p style={{ color: "#3271ED", padding: "6px 12px", margin: 0, }}> {children} </p>
    </div>
)