import React from 'react'

import { Img } from 'react-image'
import Styles from "../../styles/infoPacil"
import data from '../../data/pacil-bo.json'
import Card from 'react-bootstrap/Card'
import CardDeck from 'react-bootstrap/CardDeck'
import Carousel from 'react-bootstrap/Carousel'
import Spinner from 'react-bootstrap/Spinner'

export default function badanOtonom() {
    const spinner = <Spinner variant="warning" animation="border" />

    const matkulCard = (name, title, desc, socmed) => {
        return (
            <Card key={name} className="subcard" style={{width: "100%"}}>
                <div className="header flex-small" style={{display: "flex", gap: 8, alignItems: "center"}}>
                    <Img alt="" src={ require(`../../assets/iconlogo/${name}.png`)} style={{width:100, height: 100, paddingBottom: 8, margin: 0, objectFit: "contain"}} loader={spinner}/>

                    <h3 style={{fontWeight: "bold", color: "#352B48", marign: 0, padding: 0}}>{title}</h3>
                </div>
                {desc.map((item, key) => <p style={{color: "#534764", margin: 0}} key = {key}> {item} </p> )}

                {Object.keys(socmed).length ? <div style={{ display: "flex", flexWrap: "wrap", gap: "1em" }}>
                {socmed.twitter && <Chips imageIcon={<Img alt="" src={require(`../../assets/iconlogo/TwitterIcon.svg`)} style={{ width: 15, height: 15, }} loader={spinner} />
                            }> {socmed.twitter} </Chips>}
                            {socmed.ig && <Chips imageIcon={<Img alt="" src={require(`../../assets/iconlogo/InstagramIcon.svg`)} style={{ width: 15, height: 15, }} loader={spinner} />
                            }> {socmed.ig} </Chips>}
                            {socmed.fb && <Chips imageIcon={<Img alt="" src={require(`../../assets/iconlogo/FacebookIcon.svg`)} style={{ width: 15, height: 15, }} loader={spinner} />
                            }> {socmed.fb} </Chips>}
                            {socmed.web && <Chips imageIcon={<Img alt="" src={require(`../../assets/iconlogo/WebIcon.svg`)} style={{ width: 15, height: 15, }} loader={spinner} />
                            }> {socmed.web} </Chips>}
                            {socmed.line && <Chips imageIcon={<Img alt="" src={require(`../../assets/iconlogo/LineIcon.svg`)} style={{ width: 15, height: 15, }} loader={spinner} />
                            }> {socmed.line} </Chips>}
                            {socmed.hp && <Chips imageIcon={<Img alt="" src={require(`../../assets/iconlogo/PhoneIcon.svg`)} style={{ width: 15, height: 15, }} loader={spinner} />
                            }> {socmed.hp} </Chips>}
                </div> : null}
            </Card>
        )
    }

    const displayCarousel = (term) => {
        return (
            <div>
                <Carousel controls indicators className="carousel-xl">
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            { term.slice(0,2).map( item => matkulCard(item.name, item.title, item.desc, item.socmed))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            { term.slice(2,4).map( item => matkulCard(item.name, item.title, item.desc, item.socmed))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            { term.slice(4,6).map( item => matkulCard(item.name, item.title, item.desc, item.socmed))}
                        </CardDeck>
                    </Carousel.Item>
                </Carousel>
                <Carousel controls indicators className="carousel-lg">
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            { term.slice(0,2).map( item => matkulCard(item.name, item.title, item.desc, item.socmed))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            { term.slice(2,4).map( item => matkulCard(item.name, item.title, item.desc, item.socmed))}
                        </CardDeck>
                    </Carousel.Item>
                    <Carousel.Item>
                        <CardDeck className="my-3">
                            { term.slice(4,6).map( item => matkulCard(item.name, item.title, item.desc, item.socmed))}
                        </CardDeck>
                    </Carousel.Item>
                </Carousel>
                <Carousel controls={false} indicators className="carousel-md">
                    { term.map( item => (
                        <Carousel.Item key={item.title}>
                            {matkulCard(item.name, item.title, item.desc, item.socmed)}
                        </Carousel.Item>
                    ))}
                </Carousel>
                <Carousel controls={false} indicators className="carousel-sm">
                    { term.map( item => (
                        <Carousel.Item key={item.title}>
                            {matkulCard(item.name, item.title, item.desc, item.socmed)}
                        </Carousel.Item>
                    ))}
                </Carousel>
            </div>
        )
    }

    const displayTerm = () => {
        const { bo } = data
        return displayCarousel(bo)
    }

    return (
        <Styles>
            <div style={{width: "100%", backgroundColor: "#FDF7FF", borderRadius: 4, gap: 16, padding: 32, alignItems: "center", flexDirection: "column"}} className = "flex-tight flex-center" id = "badanOtonom">
                <h2 style = {{color: "#352B48"}} className="real-h2 h2-center"> Organisasi dan Badan Otonom </h2>
                {displayTerm()}
            </div>
        </Styles>
    )
}

const Chips = ({ imageIcon, children }) => (
    <div style={{ borderRadius: 30, border: "2px solid #3271ED", width: "fit-content", display: "flex", alignItems: "center", padding: "0px 12px" }}>
        {imageIcon}
        <p style={{ color: "#3271ED", padding: 0, margin: 0, borderLeft: "1px solid #3271ED", marginLeft: 6, paddingLeft: 6 }}> {children} </p>
    </div>
)