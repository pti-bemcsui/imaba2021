import React from 'react'
import { NavLink } from 'react-router-dom'

import Container from 'react-bootstrap/Container'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import { Img } from 'react-image'

import '../styles/navbar.css'

const selected = {
    color: "#FDF7FF"
}

const navbar = (props) => {
    return (
        <Navbar variant="dark" expand="xl" fixed="top" style={{ backgroundColor: "#352B48", color: "#FDF7FF" }}>
            <Container className="d-flex justify-content-between">
                <NavLink to="/" style={{ textDecoration: "none" }}>
                    <div style={{ display: "flex", gap: 8, alignItems: "center", paddingTop: 8, paddingBottom: 8 }}>
                        <Img alt="" src={require("../assets/iconlogo/PTI.svg")} height={45} width={45} />
                        <h1 style={{ fontSize: 18, color: "#E5DFEE", padding: 0, margin: 0 }}> BEM FASILKOM UI </h1>
                    </div>
                </NavLink>

                {!props.welcome && (
                    <React.Fragment>
                        <Navbar.Toggle aria-controls="navbar" />
                        <Navbar.Collapse id="navbar" className="justify-content-end">
                            <Nav>
                                <Nav.Item>
                                    <NavLink to="/" activeStyle={selected}>Info Kampus</NavLink>
                                </Nav.Item>
                                <Nav.Item>
                                    <NavLink to="/pacil/akademik" activeStyle={selected}>Info Akademik</NavLink>
                                </Nav.Item>

                                <Nav.Item>
                                    <NavLink to="/pacil/non-akademik" activeStyle={selected}>Info Non-Akademik</NavLink>
                                </Nav.Item>
                                <Nav.Item>
                                    <NavLink to="/pacil/narahubung" activeStyle={selected}>Kanal Narahubung</NavLink>
                                </Nav.Item>
                            </Nav>
                        </Navbar.Collapse>
                    </React.Fragment>
                )}
            </Container>
        </Navbar>
    )
}

export default navbar
