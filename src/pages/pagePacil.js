import React from "react"
import { Component } from "react"
import Hero from '../components/pagePacil/hero'

import Navbar from '../components/navbar'
import Footer from '../components/footer'


export default class pagePacil extends Component {
    componentDidMount() {
        window.scrollTo(0, 0)
    }

    render() {
        return (
            <div style={{display: "flex", flexDirection: "column", backgroundColor: "#E5DFEE", minHeight: "100vh", position: "relative"}}>
                <Navbar />
                <Hero />
                <div className = "hero-space" style={{ flex: 1 }} /> {/* Create a gap of 20% */}
                <Footer />
            </div>
        )
    }
}