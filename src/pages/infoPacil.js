import React, { Component } from 'react'

import Navbar from '../components/navbar'
import Footer from '../components/footer'
import Hero from "../components/infoPacil/hero"
import Budaya from "../components/infoPacil/budaya"
import MarsYelYel from "../components/infoPacil/yelyel"
import FunFacts from "../components/infoPacil/funfacts"
import Organisasi from "../components/infoPacil/badanOtonom"
import Kepanitiaan from "../components/infoPacil/kepanitiaan"
import UKORdanUKF from '../components/infoPacil/ukor-ukf'
import PacilLife from "../components/infoPacil/pacilLife"
import Ngoding101 from "../components/infoPacil/ngoding101"
import Denah from "../components/infoPacil/denah"

const menu = [
    {   
        display: "Budaya",
        id: "budaya"
    },
    {
        display: "Living in Fasilkom",
        id: "pacilLife"
    },
    {
        display: "Ngoding 101",
        id: "ngoding101"
    },
    // {
    //     display: "Denah",
    //     id: "denah",
    //     options: [
    //         {
    //             display: "Gedung Lama",
    //             id: "gedlam"
    //         },
    //         {
    //             display: "Gedung Baru",
    //             id: "gedbar"
    //         }
    //     ]
    // },
    {
        display: "Mars dan Yel-yel",
        id: "yelyel"
    },
    {
        display: "7 Fun Facts Fasilkom",
        id: "funfacts"
    },
    {
        display: "Organisasi dan Badan Otonom",
        id: "badanOtonom"
    },
    {
        display: "Kepanitiaan",
        id: "kepanitiaan"
    },
    {
        display: "Info UKM",
        id: "ukor-ukf"
    },
]

export default class infoPacil extends Component {
    componentDidMount() {
        window.scrollTo(0, 0)
    }
      
    render() {
        return (
            <div style={{display: "flex", flexDirection: "column", backgroundColor: "#E5DFEE", minHeight: "100vh", position: "relative", alignItems: "center"}}>
                <Navbar />
                <Hero />
                <div style={{display: 'flex', backgroundColor: "#E5DFEE", padding: 24, maxWidth: 1400, width: "100%", gap: 16}} className = "flex-normal flex-center">
                    <div style={{display: "flex", backgroundColor: "#FDF7FF", flexDirection: "column", padding: 20, minWidth: 200,  borderRadius: 4, height: "fit-content", position: "sticky"}} className = "divide-y-4 divider-container">
                        {menu.map((item) => (
                            <li style={{listStyle: "none", color: "#352B48", cursor: "pointer"}} key = {item.id} onClick={() => {
                                const element = document.getElementById(item.id);
                                const headerOffset = 100;
                                const elementPosition = element.getBoundingClientRect().top;
                                const offsetPosition = elementPosition + window.scrollY - headerOffset;
                                
                                window.scrollTo({
                                    top: offsetPosition,
                                    behavior: "smooth"
                                })
                            }}>
                                {item.display}
                                <div style={{display: "flex", flexDirection: "column", marginLeft: 12}}>
                                {item.options && item.options.map((opt, key) => (
                                    <span key = {key} onClick={() => {
                                        const element = document.getElementById(opt.id);
                                        const headerOffset = 100;
                                        const elementPosition = element.getBoundingClientRect().top;
                                        const offsetPosition = elementPosition + window.scrollY - headerOffset;
                                        
                                        window.scrollTo({
                                            top: offsetPosition,
                                            behavior: "smooth"
                                        })
                                    }}> {opt.display} </span>
                                ))}
                                </div>
                            </li>
                        ))}
                    </div>
                    <div style={{display: "flex", width: "100%", flexDirection: "column", gap: 32}}>
                        <Budaya />
                        <PacilLife />
                        <Ngoding101 />
                        <Denah />
                        <MarsYelYel />
                        <FunFacts />
                        <Organisasi />
                        <Kepanitiaan />
                        <UKORdanUKF />
                    </div>
                </div>
                <div className = "hero-space" style={{ flex: 1 }} /> {/* Create a gap of 20% */}
                <Footer />
            </div>
        )
    }
}