import React, { Component } from 'react'

import Navbar from '../components/navbar'
import Footer from '../components/footer'
import Hero from '../components/infoAkademik/hero'
import BelajarApa from '../components/infoAkademik/belajarApa'
import IlkomSI from "../components/infoAkademik/ilkomSi"
import Kurikulum from "../components/infoAkademik/kurikulum"
import Scele from "../components/infoAkademik/scele"
import Matkul from "../components/infoAkademik/matkul"

const menu = [
    {   
        display: "Belajar Apa",
        id: "belajar"
    },
    {
        display: "IK dan SI",
        id: "difference"
    },
    {
        display: "Mata Kuliah",
        id: "matkul"
    },
    {
        display: "Kurikulum",
        id: "kurikulum"
    },
    {
        display: "Scele",
        id: "scele"
    }
]

export default class infoAkademik extends Component {
    componentDidMount() {
        window.scrollTo(0, 0)
      }

    render() {
        return (
            <div style={{display: "flex", flexDirection: "column", backgroundColor: "#E5DFEE", minHeight: "100vh", position: "relative", alignItems: "center"}}>
                <Navbar />
                <Hero />
                <div style={{display: 'flex', backgroundColor: "#E5DFEE", padding: 24, maxWidth: 1400, width: "100%", gap: 16}} className = "flex-normal flex-center">
                    <div style={{display: "flex", backgroundColor: "#FDF7FF", flexDirection: "column", padding: 20, minWidth: 200,  borderRadius: 4, height: "fit-content", position: "sticky"}} className = "divide-y-4 divider-container">
                        {menu.map((item) => (
                            <li style={{listStyle: "none", color: "#352B48", cursor: "pointer"}} key = {item.id} onClick={() => {
                                const element = document.getElementById(item.id);
                                const headerOffset = 100;
                                const elementPosition = element.getBoundingClientRect().top;
                                const offsetPosition = elementPosition + window.scrollY - headerOffset;
                                
                                window.scrollTo({
                                    top: offsetPosition,
                                    behavior: "smooth"
                                })
                            }}>
                                {item.display}
                            </li>
                        ))}
                    </div>
                    <div style={{display: "flex", width: "100%", flexDirection: "column", gap: 32}}>
                        <BelajarApa />
                        <IlkomSI />
                        <Matkul />
                        <Kurikulum />
                        <Scele />
                    </div>
                </div>
                <div className = "hero-space" style={{ flex: 1 }} /> {/* Create a gap of 20% */}
                <Footer />
            </div>
        )
    }
}
