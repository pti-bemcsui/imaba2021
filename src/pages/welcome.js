import React from 'react'

import Footer from '../components/footer'
import Hero from "../components/welcome/hero"
import FacutlyProgram from "../components/welcome/facultyprogram"
import Peta from "../components/welcome/peta"
import Landmark from '../components/welcome/landmark'
import BiayaHidup from "../components/welcome/biayahidup"
import Civitas from "../components/welcome/civitas"
import Beasiswa from "../components/welcome/beasiswa"
import InfoUKM from "../components/welcome/infoukm"
import Transportasi from '../components/welcome/transportasi'
import InfoIndekos from '../components/welcome/infoIndekos'

const menu = [
    {   
        display: "Fakultas dan Program",
        id: "faculty-program"
    },
    {
        display: "Peta UI",
        id: "peta"
    }, 
    {
        display: "Landmark UI",
        id: "landmark-ui"
    }, 
    {
        display: "Info Transportasi",
        id: "info-transportasi"
    }, 
    {
        display: "Info Indekos",
        id: "indekos"
    }, 
    {
        display: "Biaya Hidup",
        id: "biaya-hidup"
    }, 
    {
        display: "Fitur-fitur Civitas Academica UI",
        id: "civitas-academica"
    }, 
    {
        display: "Beasiswa",
        id: "beasiswa"
    },
    {
        display: "Info UKM",
        id: "info-ukm"
    } 
]

export default function welcome() {
    
    return (
        <div style={{display: "flex", flexDirection: "column", backgroundColor: "#EEEEDF", minHeight: "100vh", position: "relative", alignItems: "center"}}>
                <Hero />
                <div style={{display: 'flex', backgroundColor: "#EEEEDF", padding: 24, maxWidth: 1400, width: "100%", gap: 16}} className = "flex-normal flex-center">
                    <div style={{display: "flex", backgroundColor: "#FFFEF7", flexDirection: "column", padding: 20, minWidth: 200,  borderRadius: 4, height: "fit-content", position: "sticky"}} className = "divide-y-4 divider-container-main">
                        {menu.map((item) => (
                            <li style={{listStyle: "none", color: "#352B48", cursor: "pointer"}} key = {item.id} onClick={() => {
                                const element = document.getElementById(item.id);
                                const headerOffset = 100;
                                const elementPosition = element.getBoundingClientRect().top;
                                const offsetPosition = elementPosition + window.scrollY - headerOffset;
                                
                                window.scrollTo({
                                    top: offsetPosition,
                                    behavior: "smooth"
                                })
                            }}>
                                {item.display}
                                <div style={{display: "flex", flexDirection: "column", marginLeft: 12}}>
                                {item.options && item.options.map((opt, key) => (
                                    <span key = {key} onClick={() => {
                                        const element = document.getElementById(opt.id);
                                        const headerOffset = 100;
                                        const elementPosition = element.getBoundingClientRect().top;
                                        const offsetPosition = elementPosition + window.scrollY - headerOffset;
                                        
                                        window.scrollTo({
                                            top: offsetPosition,
                                            behavior: "smooth"
                                        })
                                    }}> {opt.display} </span>
                                ))}
                                </div>
                            </li>
                        ))}
                    </div>
                    <div style={{display: "flex", width: "100%", flexDirection: "column", gap: 32}}>
                        <FacutlyProgram />
                        <Peta />
                        <Landmark />
                        <Transportasi />
                        <InfoIndekos />
                        <BiayaHidup />
                        <Civitas />
                        <Beasiswa />
                        <InfoUKM />
                    </div>
                </div>
                <div className = "hero-space" style={{ flex: 1 }} /> {/* Create a gap of 20% */}
                <Footer />
            </div>
    )
}

