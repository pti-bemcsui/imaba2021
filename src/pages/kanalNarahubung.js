import React, { Component } from 'react'

import Navbar from '../components/navbar'
import Footer from '../components/footer'

import Hero from "../components/kanalNarahubung/hero"
import Narahubung from "../components/kanalNarahubung/narahubung"

export default class KanalNarahubung extends Component {
    componentDidMount() {
        window.scrollTo(0, 0)
      }
      
    render() {

        return (
            <div style={{display: "flex", flexDirection: "column", backgroundColor: "#E5DFEE", minHeight: "100vh", position: "relative", alignItems: "center"}}>
                <Navbar/>
                <Hero />
                <div style={{display: 'flex', backgroundColor: "#E5DFEE", padding: 24, maxWidth: 1400, width: "100%", gap: 16}} className = "flex-normal flex-center">
                    <div style={{display: "flex", width: "100%", flexDirection: "column", gap: 32}}>
                        <Narahubung />
                    </div>
                </div>
                <div className = "hero-space" style={{ flex: 1 }} /> {/* Create a gap of 20% */}
                <Footer />
            </div>
        )
    }
}

