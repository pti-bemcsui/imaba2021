import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import WelcomePage from './pages/welcome'
import InfoAkademik from './pages/infoAkademik'
import InfoPacil from './pages/infoPacil'
import KanalNarahubung from './pages/kanalNarahubung'
import PagePacil from "./pages/pagePacil"

function App() {  
  return (
    <Router basename="/imaba">
      <Switch>
        <Route exact path="/pacil/akademik"><InfoAkademik /></Route>
        <Route exact path="/pacil/non-akademik"><InfoPacil /></Route>
        <Route exact path="/pacil/narahubung"><KanalNarahubung /></Route>
        <Route exact path="/pacil"><PagePacil /></Route>
        <Route exact path="/"><WelcomePage /></Route>
      </Switch>
    </Router>
  );
}

export default App;
