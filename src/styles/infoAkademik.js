import styled from 'styled-components'

const Styles = styled.div`
    /* background-color: #20CB91;
    min-height: 100vh; */

    .subcard {
        width: 100%;
        background: transparent;
        border-radius: 4px;
        padding: 1em;
        box-shadow: none;
        border: 1.5px solid #E5DFEE;
        display: flex;
        flex-direction: column;
        gap: 16px;
    
        p {
            font-weight: normal;
            font-size: 15px;
            line-height: 120%;
            color: #534764
        }
    }

    #narahubung {
        .subcard {
                width: 100%;
                background: #FDF7FF;
                border-radius: 4px;
                padding: 1em;
                box-shadow: none;
                border: 1.5px solid #E5DFEE;
                display: flex;
                flex-direction: column;
                gap: 16px;
                
                p {
                    font-weight: normal;
                    font-size: 15px;
                    line-height: 120%;
                    color: #534764
                }
        }
        p {
            text-align: justify;
            line-height: 150%;
        }

        .carousel-indicators li {
            background-color: #AFA8BA;
        }

        .badge {
            background-color: transparent;
            color: #FB6700;
            border: 0.6px solid #FB6700;
        }

        .carousel-xl, .carousel-lg {
            .card-deck  {
                width: 100%;
                min-height: 300px;
                min-width: 1000px;
            }
        }

        .carousel-lg {
            .card-deck  {
                width: 100%;
                min-height: 300px;
                min-width: 800px;
            }
        }
        .carousel-xl {
            .card-deck { min-height: 355px; }
        }

        .carousel-control-prev {
            left: -60px; /* Adjust as needed */
            }

        .carousel-control-next {
            right: -60px; /* Adjust as needed */
        }

        .carousel-xl, .carousel-lg, .carousel-md { display: none }
        .carousel-sm { 
            display: block;
            margin: 0 1rem;
            .card {
                margin-bottom: 1rem;
            }
        }
        .comingsoon { min-height: 340px }
        @media (min-width: 768px) {
            .carousel-xl, .carousel-lg, .carousel-sm { display: none }
            .carousel-md { display: block }
        }
        @media (min-width: 992px) {
            .carousel-xl, .carousel-md, .carousel-sm { display: none }
            .carousel-lg { display: block }
            .comingsoon { min-height: 300px }
        }
        @media (min-width: 1200px) {
            .carousel-lg, .carousel-md, .carousel-sm { display: none }
            .carousel-xl { display: block }
            .comingsoon { min-height: 300px }
        }
        @media (max-width:375px) {
            padding: 1rem 0;
            .comingsoon { margin: 1rem }
        }
    }

    #kurikulum {
        img {
            width: 90%;
            margin: 5%;
        }
        .btnwrapper button { margin: 5px }
    }

    .carousel-control-next, .carousel-control-prev{
        width: 5%;
    }

    .carousel-control-next-icon, .carousel-control-prev-icon {
        width: 40px;
        height: 40px;

        @media (max-width:576px) {
            display: none;
        }
    }

    .carousel-control-next-icon {
        background-image: url(${require(`../assets/infoAkademik/arrow-circle-right.svg`)});
    }
    .carousel-control-prev-icon {
        background-image: url(${require(`../assets/infoAkademik/arrow-circle-left.svg`)});
    }

    .carousel-indicators li {
        background-color: #AFA8BA;
    }

    ol {
        margin-bottom: -.5rem;
    }

    @media (min-width: 1200px) {
        ol {
            margin-bottom: -1rem;
        }
    }

    #matkul {
        p {
            text-align: justify;
            line-height: 150%;
        }

        .carousel-xl, .carousel-lg {
            .card-deck  {
                width: 90%;
                margin: 0 5%;
                min-height: 300px;
            }
        }
        .carousel-xl {
            .card-deck { min-height: 355px; }
            // .term1 {
            //     width: 35%; 
            //     margin: 0 32.5%;
            // }
            .term1, .term2 {
                width: 70%; 
                margin: 0 15%;
            }
        }
        // .carousel-lg .term1, 
        // .carousel-md .term1 {
        //     width: 50%;
        //     margin: 0 25%;             
        // }
        .carousel-xl, .carousel-lg, .carousel-md { display: none }
        .carousel-sm { 
            display: block;
            margin: 0 1rem;
            .card {
                margin-bottom: 1rem;
            }
        }
        .comingsoon { min-height: 340px }
        @media (min-width: 768px) {
            .carousel-xl, .carousel-lg, .carousel-sm { display: none }
            .carousel-md { display: block }
        }
        @media (min-width: 992px) {
            .carousel-xl, .carousel-md, .carousel-sm { display: none }
            .carousel-lg { display: block }
            .comingsoon { min-height: 300px }
        }
        @media (min-width: 1200px) {
            .carousel-lg, .carousel-md, .carousel-sm { display: none }
            .carousel-xl { display: block }
            .comingsoon { min-height: 300px }
        }
        @media (max-width:375px) {
            padding: 1rem 0;
            .comingsoon { margin: 1rem }
        }
    }

    #civitas-academica {
        p {
            text-align: justify;
            line-height: 150%;
        }

        .carousel-indicators li {
            background-color: #DEDEB8;
        }

        .badge {
            background-color: transparent;
            color: #FB6700;
            border: 0.6px solid #FB6700;
        }

        .carousel-xl, .carousel-lg {
            .card-deck  {
                width: 90%;
                margin: 0 5%;
                min-height: 300px;
            }
        }
        .carousel-xl {
            .card-deck { min-height: 355px; }
            // .term1 {
            //     width: 35%; 
            //     margin: 0 32.5%;
            // }
            .term1, .term2 {
                width: 70%; 
                margin: 0 15%;
            }
        }
        // .carousel-lg .term1, 
        // .carousel-md .term1 {
        //     width: 50%;
        //     margin: 0 25%;             
        // }
        .carousel-xl, .carousel-lg, .carousel-md { display: none }
        .carousel-sm { 
            display: block;
            margin: 0 1rem;
            .card {
                margin-bottom: 1rem;
            }
        }
        .comingsoon { min-height: 340px }
        @media (min-width: 768px) {
            .carousel-xl, .carousel-lg, .carousel-sm { display: none }
            .carousel-md { display: block }
        }
        @media (min-width: 992px) {
            .carousel-xl, .carousel-md, .carousel-sm { display: none }
            .carousel-lg { display: block }
            .comingsoon { min-height: 300px }
        }
        @media (min-width: 1200px) {
            .carousel-lg, .carousel-md, .carousel-sm { display: none }
            .carousel-xl { display: block }
            .comingsoon { min-height: 300px }
        }
        @media (max-width:375px) {
            padding: 1rem 0;
            .comingsoon { margin: 1rem }
        }
    }

    #info-transportasi {
        p {
            text-align: justify;
            line-height: 150%;
        }

        .carousel-indicators li {
            background-color: #DEDEB8;
        }

        .badge {
            background-color: transparent;
            color: #FB6700;
            border: 0.6px solid #FB6700;
        }

        .carousel-xl, .carousel-lg {
            .card-deck  {
                width: 90%;
                margin: 0 5%;
                min-height: 300px;
            }
        }
        .carousel-xl {
            .card-deck { min-height: 355px; }
            // .term1 {
            //     width: 35%; 
            //     margin: 0 32.5%;
            // }
            .term1, .term2 {
                width: 70%; 
                margin: 0 15%;
            }
        }
        // .carousel-lg .term1, 
        // .carousel-md .term1 {
        //     width: 50%;
        //     margin: 0 25%;             
        // }
        .carousel-xl, .carousel-lg, .carousel-md { display: none }
        .carousel-sm { 
            display: block;
            margin: 0 1rem;
            .card {
                margin-bottom: 1rem;
            }
        }
        .comingsoon { min-height: 340px }
        @media (min-width: 768px) {
            .carousel-xl, .carousel-lg, .carousel-sm { display: none }
            .carousel-md { display: block }
        }
        @media (min-width: 992px) {
            .carousel-xl, .carousel-md, .carousel-sm { display: none }
            .carousel-lg { display: block }
            .comingsoon { min-height: 300px }
        }
        @media (min-width: 1200px) {
            .carousel-lg, .carousel-md, .carousel-sm { display: none }
            .carousel-xl { display: block }
            .comingsoon { min-height: 300px }
        }
        @media (max-width:375px) {
            padding: 1rem 0;
            .comingsoon { margin: 1rem }
        }
    }

    #ilkomSI {
        img {
            width: 90%;
            margin: 0 5%;
        }
    }

    #scele {
        p { text-align: justify }
        .btn {
            margin: 10px; 
        }

        @media (max-width: 767px) {
            .row {
                flex-direction: column-reverse; 
            }
            .asset {
                margin-bottom: 1.5rem;
            }
        }
    }
`


export default Styles

